﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using MyBotModern;
using System.Linq;

namespace MyBotModern
{
    /// <summary>
    /// Logika interakcji dla klasy MSGBOX.xaml
    /// </summary>
    public partial class MSGBOX : Window
    {
        public enum Display
        {
            Potion,
            Skill,
            Buff,
        }
        public Display Dis { get; set; }
        //public int Skillbar { get; set; }
        //public Key Key { get; set; }
        //public new int Effect { get; set; }
        //public int Percent { get; set; }
        //public int Cooldown { get; set; }
        //public int CastTime { get; set; }
        //public int ChainLVL { get; set; }
        //public int ChainWait { get; set; }
        //public bool OnCombat { get; set; }

        public MSGBOX(Display a)
        {
            Dis = a;
            this.Topmost = true;
            this.CommandBindings.Add(new CommandBinding(SystemCommands.CloseWindowCommand, this.OnCloseWindow));
            InitializeComponent();
            CBeffect.ItemsSource = Enum.GetValues(typeof(Enums.PotionEffect)).Cast<Enums.PotionEffect>();
            if (a == Display.Potion)
            {
                ChainLevelPanel.Visibility = Visibility.Collapsed;
                ChainWaitPanel.Visibility = Visibility.Collapsed;
                OnCombatPanel.Visibility = Visibility.Collapsed;
            }
            else if (a == Display.Skill || a == Display.Buff)
            {
                EffectPanel.Visibility = Visibility.Collapsed;
                PercentPanel.Visibility = Visibility.Collapsed;
                if (a == Display.Skill)
                    OnCombatPanel.Visibility = Visibility.Collapsed;
            }

        }

        private void BTNcancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }


        private void OnCloseWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.CloseWindow(this);
        }

        private void BTok_Click(object sender, RoutedEventArgs e)
        {
            if (Dis == Display.Potion)
            {
                Potion.list.Add(new PotionItem(CBskillbar.SelectedIndex,
                    (Key)KeyComboBox.SelectedItem,
                    NRcooldown.Value,
                    NRcasttime.Value,
                    (Enums.PotionEffect)CBeffect.SelectedIndex,
                    NRpercent.Value));
            }
            else if (Dis == Display.Skill)
            {
                Skill.list.Add(new SkillsItem(CBskillbar.SelectedIndex,
                    (Key)KeyComboBox.SelectedItem,
                    NRcooldown.Value,
                    NRcasttime.Value,
                    NRchainlvl.Value,
                    NRchainwait.Value));
            }
            else if (Dis == Display.Buff)
            {
                Buff.list.Add(new BuffItem(CBskillbar.SelectedIndex,
                    (Key)KeyComboBox.SelectedItem,
                    NRcooldown.Value,
                    NRcasttime.Value,
                    NRchainlvl.Value,
                    NRchainwait.Value,
                    (bool)CBonCombat.IsChecked));
            }
            this.DialogResult = true;
            SystemCommands.CloseWindow(this);
        }

        //public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        //{
        //    if (depObj != null)
        //    {
        //        for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
        //        {
        //            DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
        //            if (child != null && child is T)
        //            {
        //                yield return (T)child;
        //            }

        //            foreach (T childOfChild in FindVisualChildren<T>(child))
        //            {
        //                yield return childOfChild;
        //            }
        //        }
        //    }
        //}
    }
}
