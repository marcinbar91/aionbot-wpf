﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using static MyBotModern.Enums;

namespace MyBotModern
{
    public class Bot
    {
        static Script runningScript;
        private static BotStatus status;

        public static BotStatus Status
        {
            get { return status; }
            set
            {
                if (Status != value)
                {
                    status = value;
                    NotifyStaticPropertyChanged(value.ToString());
                }
            }
        }

        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

        private static void NotifyStaticPropertyChanged(string propertyName)
        {
            StaticPropertyChanged?.Invoke(typeof(Bot), new PropertyChangedEventArgs(propertyName));
        }



        public static void CheckScript()
        {

            //float disDead = File.Exists(Program.bot.TBdeathway.Text) ? new ScriptItem(Program.bot.TBdeathway.Text).FindNear().Distance : float.MaxValue;
            //if (disDead < Properties.Settings.Default.MaxDistance)
            //{
            //    BOTstatus = BotStatus.DeathWaypoint;
            //    return new Script(Program.bot.TBdeathway.Text);
            //}

            //float discombat = File.Exists(Program.bot.TBactionway.Text) ? new Script(Program.bot.TBactionway.Text).FindNear().Distance : float.MaxValue;
            //float disEssence = File.Exists(Program.bot.TBessencetappingway.Text) ? new Script(Program.bot.TBessencetappingway.Text).FindNear().Distance : float.MaxValue;
            //float disSafeAether = Program.bot.ListAetherSafeSpot.Items.Count > 0 ? new Script(Program.bot.ListAetherSafeSpot).FindNear().Distance : float.MaxValue;


            //float[] min2 = (new float[] { discombat, disEssence, disSafeAether }).Where(a => a < Properties.Settings.Default.MaxDistance).ToArray();

            //if (min2.Count() != 0)
            //{
            //    float min = min2.Count() > 1 ? min2[1] : min2[0];

            //    if (min == discombat)
            //    {
            //        BOTstatus = BotStatus.CombatWaypoint;
            //        return new Script(Program.bot.TBactionway.Text);
            //    }
            //    else if (min == disEssence)
            //    {
            //        BOTstatus = BotStatus.Essencetapping;
            //        return new Script(Program.bot.TBessencetappingway.Text);
            //    }
            //    else if (min == disSafeAether)
            //    {
            //        BOTstatus = BotStatus.Aethertapping;
            //        return new Script(Program.bot.TBaethertappingway.Text);
            //    }
            //    else
            //    {
            //        Log.Write("No detected suitable script", Colors.System);
            //        return null;
            //    }
            //}
            //else
            //{
            //    Log.Write("No detected suitable script", Colors.System);
            //return null;
            //}

        }


        private static bool CheckScriptsIsCorrect()
        {
            if (new Script(Program.bot.TBdeathway.Text).CheckScript())
            {
                if (new Script(Program.bot.TBactionway.Text).CheckScript())
                {
                    Script DeadScript = new Script(Program.bot.TBdeathway.Text);
                    Script actionScript = new Script(Program.bot.TBactionway.Text);

                    PointXYZ lastdead = null;
                    for (int i = DeadScript.Count - 1; i >= 0; i--)
                    {
                        if (DeadScript.Command[i] == Properties.ScriptSetting.Default.go)
                        {
                            lastdead = new PointXYZ(float.Parse(DeadScript.Parameter[i][0]), float.Parse(DeadScript.Parameter[i][1]), float.Parse(DeadScript.Parameter[i][2]));
                            break;
                        }

                    }
                    PointXYZ actionpoint = null;
                    for (int i = 0; i < actionScript.Count; i++)
                    {
                        if (actionScript.Command[i] == Properties.ScriptSetting.Default.go)
                        {
                            actionpoint = new PointXYZ(float.Parse(actionScript.Parameter[i][0]), float.Parse(actionScript.Parameter[i][1]), float.Parse(actionScript.Parameter[i][2]));
                            break;
                        }

                    }
                    if (actionpoint.Distance(lastdead) > Properties.Settings.Default.MaxDistance)
                    {
                        MessageBox.Show("Death Script does not fit to Action Script!");
                        return false;
                    }

                    if (DeadScript.FindNear().Distance >= Properties.Settings.Default.MaxDistance && actionScript.FindNear().Distance > Properties.Settings.Default.MaxDistance)
                    {
                        MessageBox.Show("Character is too far from Script!");
                        return false;
                    }
                    return true;
                }
                else
                {
                    MessageBox.Show("Action Script is incorrect or doesn't exist!");
                    return false;
                }

            }
            else
            {
                MessageBox.Show("Death Script is incorrect or doesn't exist!");
                return false;
            }
        }

        public static async void StartBot()
        {

            if (CheckScriptsIsCorrect())
            {
                Program.bot.BTNStartBot.Content = "Stop Bot";
                Script deadScript = new Script(Program.bot.TBdeathway.Text);
                Script actionScript = new Script(Program.bot.TBactionway.Text, Script.Mode.Hunting);

                if (deadScript.FindNear().Distance < actionScript.FindNear().Distance)
                {
                    Status = BotStatus.DeathWaypoint;
                    runningScript = deadScript;
                    await runningScript.Start();
                    if (!(Status == BotStatus.Idle))
                    {
                        Status = BotStatus.ActionWaypoint;
                        runningScript = actionScript;
                        await runningScript.Start(true);
                    }
                }
                else
                {

                    Status = BotStatus.ActionWaypoint;
                    runningScript = actionScript;
                    await runningScript.Start(true);
                }
            }
            else
                Log.Write("Script error!", Colors.System);
        }


        public static void Stop()
        {
            if (runningScript != null)
            {
                Status = BotStatus.Idle;
                runningScript.Stop();
                runningScript = null;

            }



        }


    }
}
