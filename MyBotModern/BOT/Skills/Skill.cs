﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBotModern
{
    class Skill
    {
        public static ObservableCollection<SkillsItem> list = new ObservableCollection<SkillsItem>();

        public bool CheckEnemyDead()
        {
            if (Value.Target.HP.Cur > 0)
                return false;
            else
                return true;
        }

        public static async Task Use()
        {
            for (int rows = 0; rows < list.Count; rows++)
            {
                if (list[rows].Cooldown < Calc.Subtract(list[rows].Date.AddSeconds(1)))
                    if (Value.Target.HP.Cur > 0)
                    {
                        Value.Player.SkillbarNumber = list[rows].Skillbar;
                        Keys.KeyPress(list[rows].Key);
                        await Task.Delay(list[rows].CastTime * 1000 + 1000);
                        list[rows].Date = DateTime.Now;
                        break;
                    }
            }

        }

        public static async Task Use2()
        {

            for (int rows = 0; rows < list.Count; rows++)
            {
                Value.Player.SkillbarNumber = list[rows].Skillbar;
                Keys.KeyPress(list[rows].Key);
                await Task.Delay(100);
                list[rows].Date = DateTime.Now;
            }

        }

    }
}
