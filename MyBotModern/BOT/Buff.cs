﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBotModern
{
    class Buff
    {
        static int skillbar;
        static string key;
        static int cooldown;
        static int casttime;
        static int chainlvl;
        static int chainwait;
        static DateTime date;
        static bool onCombat;


        public static async Task Use()
        {
            for (int rows = 0; rows < Program.bot.DGVbuff.Items.Count; rows++)
            {
                ReadSkillDataGridView(rows);
                if (cooldown < Calc.Subtract(date))
                {
                    if (Value.Target.HP.Cur == 0 || onCombat)
                    {
                        int id = Value.Skillbar.GetID(skillbar, 1, KeyToInt(key));
                        if (!IsBuffed(id))
                        {
                            do
                            {
                                Value.Player.SkillbarNumber = skillbar;
                                Keys.KeyPress(key);
                                await Task.Delay(50);
                            }
                            while (!IsBuffed(id));
                            Log.Write("Buff - " + id, Enums.Colors.Use);
                            await Task.Delay(casttime * 1000 + 1000);
                            //Program.bot.DGVbuff.Items[rows].Cells[4].Value = DateTime.Now.AddSeconds(casttime);


                        }
                    }
                }
            }

        }

        private static bool IsBuffed(int id)
        {
            List<int> list = Value.Buff.ListID;
            return list.Contains(id);
        }


        private static void ReadSkillDataGridView(int row)
        {
            //skillbar = Convert.ToInt32(Program.bot.DGVbuff.Rows[row].Cells[0].Value.ToString());
            //key = Program.bot.DGVbuff.Rows[row].Cells[1].Value.ToString();
            //cooldown = Convert.ToInt32(Program.bot.DGVbuff.Rows[row].Cells[2].Value);
            //casttime = Convert.ToInt32(Program.bot.DGVbuff.Rows[row].Cells[3].Value);
            //date = Convert.ToDateTime(Program.bot.DGVbuff.Rows[row].Cells[4].Value.ToString());
            //chainlvl = Convert.ToInt32(Program.bot.DGVbuff.Rows[row].Cells[5].Value);
            //chainwait = Convert.ToInt32(Program.bot.DGVbuff.Rows[row].Cells[6].Value);
            //onCombat = Convert.ToBoolean(Program.bot.DGVbuff.Rows[row].Cells[7].Value);

        }


        private static int KeyToInt(string key)
        {
            if (key == "D0")
                return 10;
            else if (key == "D1")
                return 1;
            else if (key == "D2")
                return 2;
            else if (key == "D3")
                return 3;
            else if (key == "D4")
                return 4;
            else if (key == "D5")
                return 5;
            else if (key == "D6")
                return 6;
            else if (key == "D7")
                return 7;
            else if (key == "D8")
                return 8;
            else if (key == "D9")
                return 9;
            else if (key == "OemMinus")
                return 11;
            else if (key == "Oemplus")
                return 12;

            return 0;
        }

    }
}
