﻿using System;
using System.Windows.Input;

namespace MyBotModern
{
    public class PotionItem : Enums
    {
        public int Skillbar { get; set; }
        public Key Key { get; set; }
        public int Cooldown { get; set; }
        public int CastTime { get; set; }
        public PotionEffect Effect { get; set; }
        public int Percent { get; set; }
        public DateTime Date { get; set; }


        public PotionItem(int skillbar, Key key, int cooldown, int castTime, PotionEffect effect, int percent)
        {
            Skillbar = skillbar;
            Key = key;
            Cooldown = cooldown;
            CastTime = castTime;
            Effect = effect;
            Percent = percent;
        }
        //public int skillbar = 1;
        //public Key key = System.Windows.Input.Key.D1;
        //public int cooldown = 1;
        //public int castTime = 1;
        //public int effect = 1;
        //public int percent = 99;


        //public int Skillbar
        //{
        //    get { return skillbar; }
        //    set
        //    {skillbar = value;
        //    }
        //}

        //public Key Key
        //{
        //    get { return key; }
        //    set
        //    {
        //        key = value;
        //    }
        //}

        //public int Cooldown
        //{
        //    get { return cooldown; }
        //    set
        //    {
        //        cooldown = value;
        //    }
        //}
        //public int CastTime
        //{
        //    get { return castTime; }
        //    set
        //    {
        //        castTime = value;
        //    }
        //}
        //public PotionEffect Effect
        //{
        //    get { return effect; }
        //    set { effect = value; }
        //}
        //public int Percent
        //{
        //    get { return percent; }
        //    set { percent = value; }
        //}



    }
}
