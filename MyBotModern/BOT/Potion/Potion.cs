﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace MyBotModern
{
    public class Potion : Enums
    {

        public static ObservableCollection<PotionItem> list = new ObservableCollection<PotionItem>();

        public static async Task Use()
        {
            for (int rows = 0; rows < list.Count; rows++)
            {
                if (list[rows].Effect == PotionEffect.HP)
                {
                    if (Calc.Percent(Value.Player.HP.Cur, Value.Player.HP.Max) <= list[rows].Percent)
                        if (list[rows].Cooldown < Calc.Subtract(list[rows].Date.AddSeconds(1)))
                        {
                            int temp = Value.Player.HP.Cur;
                            do
                            {
                                Value.Player.SkillbarNumber = list[rows].Skillbar;
                                Keys.KeyPress(list[rows].Key);
                                await Task.Delay(50);
                            } while (Value.Player.HP.Cur <= temp && Program.bot.CBPotionCheck.IsChecked.Value);
                            Log.Write("Potion - HP", Colors.Use);
                            await Task.Delay(list[rows].CastTime * 1000);
                            list[rows].Date = DateTime.Now;
                        }
                }
                else if (list[rows].Effect == PotionEffect.MP)
                {
                    if (Calc.Percent(Value.Player.MP.Cur, Value.Player.MP.Max) <= list[rows].Percent)
                        if (list[rows].Cooldown < Calc.Subtract(list[rows].Date.AddSeconds(1)))
                        {
                            int temp = Value.Player.MP.Cur;
                            do
                            {
                                Value.Player.SkillbarNumber = list[rows].Skillbar;
                                Keys.KeyPress(list[rows].Key);
                                await Task.Delay(50);
                            } while (Value.Player.MP.Cur <= temp && Program.bot.CBPotionCheck.IsChecked.Value);
                            Log.Write("Potion - MP", Colors.Use);
                            await Task.Delay(list[rows].CastTime * 1000);
                            list[rows].Date = DateTime.Now;
                        }
                }
                else if (list[rows].Effect == PotionEffect.Vigor)
                {
                    if (Calc.Percent(Value.Player.HP.Cur, Value.Player.HP.Max) < list[rows].Percent && Calc.Percent(Value.Player.MP.Cur, Value.Player.MP.Max) < list[rows].Percent)
                        if (list[rows].Cooldown < Calc.Subtract(list[rows].Date.AddSeconds(1)))
                        {
                            int temp = Value.Player.HP.Cur;
                            int temp2 = Value.Player.MP.Cur;
                            do
                            {
                                Value.Player.SkillbarNumber = list[rows].Skillbar;
                                Keys.KeyPress(list[rows].Key);
                                await Task.Delay(50);
                            } while ((Value.Player.HP.Cur <= temp && Value.Player.MP.Cur <= temp2) && Program.bot.CBPotionCheck.IsChecked.Value);
                            Log.Write("Potion - Vigor", Colors.Use);
                            await Task.Delay(list[rows].CastTime * 1000);
                            list[rows].Date = DateTime.Now;
                        }
                }
                else if (list[rows].Effect == PotionEffect.Flight)
                {
                    if (Calc.Percent(Value.Player.Flight.Cur, Value.Player.Flight.Max) <= list[rows].Percent)
                        if (list[rows].Cooldown < Calc.Subtract(list[rows].Date.AddSeconds(1)))
                        {
                            int temp = Value.Player.Flight.Cur;
                            do
                            {
                                Value.Player.SkillbarNumber = list[rows].Skillbar;
                                Keys.KeyPress(list[rows].Key);
                                await Task.Delay(50);
                            } while (Value.Player.Flight.Cur <= temp && Program.bot.CBPotionCheck.IsChecked.Value);
                            Log.Write("Potion - Flight", Colors.Use);
                            await Task.Delay(list[rows].CastTime * 1000);
                            list[rows].Date = DateTime.Now;
                        }
                }
            }
        }
        public static async Task Rest()
        {
            if (Program.bot.chkEnableRest.IsChecked.Value)
            {
                if (Calc.Percent(Value.Player.HP.Cur, Value.Player.HP.Max) < Program.bot.NRHPpercent.Value || Calc.Percent(Value.Player.MP.Cur, Value.Player.MP.Max) < Program.bot.NRMPpercent.Value)
                {
                    Keys.Rest();
                    do
                    {
                        if (Value.Target.HP.Cur > 0)
                        {
                            await Task.Delay(1000);
                            Keys.Rest();
                            return;
                        }
                        await Task.Delay(1000);
                    } while (Value.Player.HP.Cur != Value.Player.HP.Max || Value.Player.MP.Cur != Value.Player.MP.Max);

                    Keys.Rest();
                }
            }
        }
        public static async Task RestFlight()
        {
            do
            {
                if (Value.Target.HP.Cur > 0)
                {
                    await Task.Delay(1000);
                    return;
                }
                await Task.Delay(1000);
            } while (Value.Player.Flight.Cur != Value.Player.Flight.Max);
        }

    }
}
