﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MyBotModern
{
    class Buff
    {
        public static ObservableCollection<BuffItem> list = new ObservableCollection<BuffItem>();


        public static async Task Use()
        {
            for (int rows = 0; rows < list.Count; rows++)
            {
                if (list[rows].Cooldown < Calc.Subtract(list[rows].Date))
                {
                    if (Value.Target.HP.Cur == 0 || list[rows].OnCombat)
                    {
                        int id = Value.Skillbar.GetID(list[rows].Skillbar, 1, KeyToInt(list[rows].Key));
                        if (!IsBuffed(id))
                        {
                            do
                            {
                                Value.Player.SkillbarNumber = list[rows].Skillbar;
                                Keys.KeyPress(list[rows].Key);
                                await Task.Delay(50);
                            }
                            while (!IsBuffed(id));
                            Log.Write("Buff - " + id, Enums.Colors.Use);
                            await Task.Delay(list[rows].CastTime * 1000 + 1000);
                            list[rows].Date = DateTime.Now.AddSeconds(list[rows].CastTime);
                        }
                    }
                }
            }

        }

        private static bool IsBuffed(int id)
        {
            List<int> list = Value.Buff.ListID;
            return list.Contains(id);
        }


        private static int KeyToInt(Key key)
        {
            if (key == Key.D1)
                return 1;
            else if (key == Key.D2)
                return 2;
            else if (key == Key.D3)
                return 3;
            else if (key == Key.D4)
                return 4;
            else if (key == Key.D5)
                return 5;
            else if (key == Key.D6)
                return 6;
            else if (key == Key.D7)
                return 7;
            else if (key == Key.D8)
                return 8;
            else if (key == Key.D9)
                return 9;
            else if (key == Key.D0)
                return 10;
            else if (key == Key.OemMinus)
                return 11;
            else if (key == Key.OemPlus)
                return 12;

            return 0;
        }

    }
}
