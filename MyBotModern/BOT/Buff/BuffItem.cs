﻿using System;
using System.Windows.Input;

namespace MyBotModern
{
    public class BuffItem
    {
        public int Skillbar { get; set; }
        public Key Key { get; set; }
        public int Cooldown { get; set; }
        public int CastTime { get; set; }
        public int ChainLevel { get; set; }
        public int ChainWait { get; set; }
        public bool OnCombat { get; set; }
        public DateTime Date { get; set; }

        public BuffItem(int skillbar, Key key, int cooldown, int castTime, int chainLevel, int chainWait, bool onCombat)
        {
            Skillbar = skillbar;
            Key = key;
            Cooldown = cooldown;
            CastTime = castTime;
            ChainLevel = chainLevel;
            ChainWait = chainWait;
            OnCombat = onCombat;
        }
    }
}
