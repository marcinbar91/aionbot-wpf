﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace MyBotModern
{
    class RefreshForm
    {
        private static byte pdp = 0;
        private static byte pclass;

        public static void Do()
        {
            Program.bot.Dispatcher.Invoke(new Action(delegate ()
            {
                //Program.bot.LBloggin.Text = "Loggin " + Value.Player.Loggin;

                if (pclass != Value.Player.Class)
                    switch (Value.Player.Class)
                    {
                        case 0:
                            Program.bot.PNLclass.Source = Imaging.CreateBitmapSourceFromHBitmap(Properties.Resources.Warrior.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                            pclass = 0;
                            break;
                        case 1:
                            Program.bot.PNLclass.Source = Imaging.CreateBitmapSourceFromHBitmap(Properties.Resources.Gladiator.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                            pclass = 1;
                            break;
                        case 2:
                            Program.bot.PNLclass.Source = Imaging.CreateBitmapSourceFromHBitmap(Properties.Resources.Templar.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                            pclass = 2;
                            break;
                        case 3:
                            Program.bot.PNLclass.Source = Imaging.CreateBitmapSourceFromHBitmap(Properties.Resources.Scout.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                            pclass = 3;
                            break;
                        case 4:
                            Program.bot.PNLclass.Source = Imaging.CreateBitmapSourceFromHBitmap(Properties.Resources.Assassin.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                            pclass = 4;
                            break;
                        case 5:
                            Program.bot.PNLclass.Source = Imaging.CreateBitmapSourceFromHBitmap(Properties.Resources.Ranger.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                            pclass = 5;
                            break;
                        case 6:
                            Program.bot.PNLclass.Source = Imaging.CreateBitmapSourceFromHBitmap(Properties.Resources.Mage.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                            pclass = 6;
                            break;
                        case 7:
                            Program.bot.PNLclass.Source = Imaging.CreateBitmapSourceFromHBitmap(Properties.Resources.Sorcerer.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                            pclass = 7;
                            break;
                        case 8:
                            Program.bot.PNLclass.Source = Imaging.CreateBitmapSourceFromHBitmap(Properties.Resources.Spiritmaster.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                            pclass = 8;
                            break;
                        case 9:
                            Program.bot.PNLclass.Source = Imaging.CreateBitmapSourceFromHBitmap(Properties.Resources.Priest.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                            pclass = 9;
                            break;
                        case 10:
                            Program.bot.PNLclass.Source = Imaging.CreateBitmapSourceFromHBitmap(Properties.Resources.Cleric.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                            pclass = 10;
                            break;
                        case 11:
                            Program.bot.PNLclass.Source = Imaging.CreateBitmapSourceFromHBitmap(Properties.Resources.Chanter.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                            pclass = 11;
                            break;
                        case 12:
                            Program.bot.PNLclass.Source = Imaging.CreateBitmapSourceFromHBitmap(Properties.Resources.Engineer.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                            pclass = 12;
                            break;
                        case 13:
                            Program.bot.PNLclass.Source = Imaging.CreateBitmapSourceFromHBitmap(Properties.Resources.Etertech.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                            pclass = 13;
                            break;
                        case 14:
                            Program.bot.PNLclass.Source = Imaging.CreateBitmapSourceFromHBitmap(Properties.Resources.Gunner.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                            pclass = 14;
                            break;
                        case 15:
                            Program.bot.PNLclass.Source = Imaging.CreateBitmapSourceFromHBitmap(Properties.Resources.Artist.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                            pclass = 15;
                            break;
                        case 16:
                            Program.bot.PNLclass.Source = Imaging.CreateBitmapSourceFromHBitmap(Properties.Resources.Bard.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                            pclass = 16;
                            break;
                    }

                string gb = Value.Player.Name + "-" + Value.Player.Level;
                if (Program.bot.GBplayer.Header.ToString() != gb)
                    Program.bot.GBplayer.Header = gb;

                if (Value.Target.Has == 1)
                {
                    string gbt = Value.Target.Name + "-" + Value.Target.Lvl + "    Distance: " + Value.Target.Distance;
                    if (Program.bot.GBtarget.Header.ToString() != gbt)
                        Program.bot.GBtarget.Header = gbt;
                }
                else
                    if (Program.bot.GBtarget.Header.ToString() != "No Target")
                    Program.bot.GBtarget.Header = "No Target";

                //Program.bot.LBflytype.Text = "Fly Stat: " + Value.Player.Flight.Type;

                Program.bot.PlayerHPbar.Maximum = Value.Player.HP.Max;
                Program.bot.PlayerHPbar.Value = Value.Player.HP.Cur;
                Program.bot.PlayerMPbar.Maximum = Value.Player.MP.Max;
                Program.bot.PlayerMPbar.Value = Value.Player.MP.Cur;
                Program.bot.PlayerDPbar.Maximum = Value.Player.DP.Max;
                Program.bot.PlayerDPbar.Value = Value.Player.DP.Cur;
                Program.bot.PlayerEXPbar1.Maximum = Value.Player.EXP.Max;
                Program.bot.PlayerEXPbar1.Value = Value.Player.EXP.Cur;
                Program.bot.PlayerFLYbar.Maximum = Value.Player.Flight.Max;
                Program.bot.PlayerFLYbar.Value = Value.Player.Flight.Cur;
                Program.bot.TargetHPbar.Maximum = Value.Target.HP.Max;
                Program.bot.TargetHPbar.Value = Value.Target.HP.Cur;
                Program.bot.TargetMPbar.Maximum = Value.Target.MP.Max;
                Program.bot.TargetMPbar.Value = Value.Target.MP.Cur;

                Program.bot.PlayerHPbar.Text = Value.Player.HP.Cur.ToString("N0") + " / " + Value.Player.HP.Max.ToString("N0");
                Program.bot.PlayerMPbar.Text = Value.Player.MP.Cur.ToString("N0") + " / " + Value.Player.MP.Max.ToString("N0");
                Program.bot.PlayerDPbar.Text = Value.Player.DP.Cur.ToString("N0") + " / " + Value.Player.DP.Max.ToString("N0");
                Program.bot.PlayerEXPbar1.Text = Value.Player.EXP.Cur.ToString("N0") + " /" + Value.Player.EXP.Max.ToString("N0");
                Program.bot.PlayerFLYbar.Text = Value.Player.Flight.Cur.ToString("N0") + " / " + Value.Player.Flight.Max.ToString("N0");


                Program.bot.TargetHPbar.Text = Value.Target.HP.Cur.ToString("N0") + " / " + Value.Target.HP.Max.ToString("N0");
                Program.bot.TargetMPbar.Text = Value.Target.MP.Cur.ToString("N0") + " / " + Value.Target.MP.Max.ToString("N0");

                if (Value.Player.DP.Cur >= 2000 && Value.Player.DP.Cur < 3000)
                {
                    if (pdp != 1)
                        Program.bot.PNLdplevel.Source = Imaging.CreateBitmapSourceFromHBitmap(Properties.Resources.DP1.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                    pdp = 1;
                }
                else if (Value.Player.DP.Cur >= 3000 && Value.Player.DP.Cur < 4000)
                {

                    if (pdp != 2)
                        Program.bot.PNLdplevel.Source = Imaging.CreateBitmapSourceFromHBitmap(Properties.Resources.DP2.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                    pdp = 2;
                }
                else if (Value.Player.DP.Cur == 4000)
                {

                    if (pdp != 3)
                        Program.bot.PNLdplevel.Source = Imaging.CreateBitmapSourceFromHBitmap(Properties.Resources.DP3.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                    pdp = 3;
                }
                else if (Value.Player.DP.Cur < 2000)
                {
                    pdp = 0;
                    Program.bot.PNLdplevel.Source = null;
                }
            }));
        }

        private static void GetClass(int cl)
        {

        }
    }
}
