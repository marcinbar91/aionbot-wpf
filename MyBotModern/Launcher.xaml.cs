﻿using MB.WPF.ProcessControl;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace MyBotModern
{
    /// <summary>
    /// Logika interakcji dla klasy Launcher.xaml
    /// </summary>
    public partial class Launcher : System.Windows.Window
    {
        public class AionProcessInfo
        {
            public AionProcessInfo(Process proc)
            {
                Memory mem = new Memory(proc, "Game.dll");
                ID = proc.Id;
                PlayerName = mem.Read((int)Offsets.Player.Name).ConvertToString();

            }
            public int ID { get; set; }
            public string PlayerName { get; set; }
        }

        [DllImport("kernel32.dll")]
        public static extern IntPtr OpenProcess(Enums.ProcessAccessFlags dwDesiredAccess, bool bInheritHandle, int dwProcessId);

        public static IntPtr baseAddress;
        public static IntPtr processHandle;
        public static IntPtr processHandlekey;
        public static Process aionProcess;

        DispatcherTimer dispatcherTimer = new DispatcherTimer();
        ObservableCollection<AionProcessInfo> list = new ObservableCollection<AionProcessInfo>();

        private bool ProcessOnList(int id)
        {
            foreach (AionProcessInfo s in list)
            {
                if (s.ID == id)
                    return true;
            }
            return false;
        }

        private void RefreshTimer_Tick(object sender, EventArgs e)
        {
            List<Process> ProcessList;
            ProcessList = Process.GetProcessesByName("aion.bin").ToList();

            for (int i = 0; i < ProcessList.Count; i++)
            {
                AionProcessInfo s = new AionProcessInfo(ProcessList[i]);
                if (!string.IsNullOrEmpty(s.PlayerName))
                {
                    if (!ProcessOnList(s.ID))
                        list.Add(s);
                }
                else
                {
                    if (ProcessOnList(s.ID))
                        list.RemoveAt(i);
                }
            }
            if (list.Count != 0)
            {
                DGAionProcess.SelectedIndex = 0;
                BTNstart.IsEnabled = true;
            }
            else
            {
                DGAionProcess.SelectedIndex = -1;
                BTNstart.IsEnabled = false;
            }
        }

        public Launcher()
        {
            this.CommandBindings.Add(new CommandBinding(SystemCommands.CloseWindowCommand, this.OnCloseWindow));
            this.CommandBindings.Add(new CommandBinding(SystemCommands.MinimizeWindowCommand, this.OnMinimizeWindow, this.OnCanMinimizeWindow));
            this.CommandBindings.Add(new CommandBinding(SystemCommands.RestoreWindowCommand, this.OnRestoreWindow, this.OnCanResizeWindow));
            InitializeComponent();
            DGAionProcess.DataContext = list;
            dispatcherTimer.Tick += RefreshTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimer.Start();
        }

        private void BTNlaunchaion_Click(object sender, RoutedEventArgs e)
        {
            string InstallPath = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Gameforge\AION-LIVE", "BaseDir", Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Gameforge\AION-LIVE", "BaseDir", null));
            if (InstallPath != null)
            {
                InstallPath += "bin32\\aion.bin";
                InstallPath = InstallPath.Replace(" ", "\" \"");
                RunAion(InstallPath);
            }
            else
                MessageBox.Show("Error", " Aion Not Found");
        }

        private void BTNstart_Click(object sender, RoutedEventArgs e)
        {
            Start();
        }

        private void Start()
        {
            int currentPID = list[DGAionProcess.SelectedIndex].ID;
            processHandle = OpenProcess(Enums.ProcessAccessFlags.All, false, currentPID);
            processHandlekey = Process.GetProcessById(currentPID).MainWindowHandle;
            baseAddress = GetModuleBaseAddress("Game.dll", currentPID);
            aionProcess = Process.GetProcessById(currentPID);
            Program.bot.Show();
            Program.launcher.Close();
        }

        public IntPtr GetModuleBaseAddress(string ModuleName, int currentPID)
        {
            IntPtr BaseAddress = IntPtr.Zero;
            ProcessModule myProcessModule = null;
            ProcessModuleCollection myProcessModuleCollection;

            try
            {
                myProcessModuleCollection = Process.GetProcessById(currentPID).Modules;
            }
            catch { return IntPtr.Zero; }

            for (int i = 0; i < myProcessModuleCollection.Count; i++)
            {
                myProcessModule = myProcessModuleCollection[i];
                if (myProcessModule.ModuleName.Contains(ModuleName))
                {
                    BaseAddress = myProcessModule.BaseAddress;
                    break;
                }
            }

            return BaseAddress;
        }
        static void RunAion(string command)
        {
            command += " -ip:79.110.83.80 -port:2106 -noauthgg -noweb -lang:eng";
            ProcessStartInfo processInfo = new ProcessStartInfo("cmd.exe", "@/c " + command);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;

            Process process = Process.Start(processInfo);

            process.Close();
        }

        private void OnCanResizeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = this.ResizeMode == ResizeMode.CanResize || this.ResizeMode == ResizeMode.CanResizeWithGrip;
        }

        private void OnCanMinimizeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = this.ResizeMode != ResizeMode.NoResize;
        }

        private void OnCloseWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.CloseWindow(this);
        }

        private void OnMaximizeWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MaximizeWindow(this);
        }

        private void OnMinimizeWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MinimizeWindow(this);
        }

        private void OnRestoreWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.RestoreWindow(this);
        }
    }
}
