﻿using MB.WPF.ProcessControl;
//using Microsoft.Samples.CustomControls;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;


namespace MyBotModern
{
    public partial class MyBot : Window
    {
        TargetInfo target = new TargetInfo();
        PlayerInfo player = new PlayerInfo();
        public static Bot botcontrol = new Bot();
        Thread threadStart;
        static bool stopBot = false;
        WindowControl window = new WindowControl(Launcher.processHandlekey);

        System.Windows.Forms.NotifyIcon ni = new System.Windows.Forms.NotifyIcon();

        public MyBot()
        {
            this.CommandBindings.Add(new CommandBinding(SystemCommands.CloseWindowCommand, this.OnCloseWindow));
            this.CommandBindings.Add(new CommandBinding(SystemCommands.MaximizeWindowCommand, this.OnMaximizeWindow, this.OnCanResizeWindow));
            this.CommandBindings.Add(new CommandBinding(SystemCommands.MinimizeWindowCommand, this.OnMinimizeWindow, this.OnCanMinimizeWindow));
            this.CommandBindings.Add(new CommandBinding(SystemCommands.RestoreWindowCommand, this.OnRestoreWindow, this.OnCanResizeWindow));

            ni.Icon = Properties.Resources.icon;
            ni.Visible = true;
            ni.BalloonTipTitle = "MyBot";
            ni.BalloonTipText = "Here is your MyBot";
            ni.ShowBalloonTip(500);
            ni.DoubleClick +=
                delegate (object sender, EventArgs args)
                {
                    this.Show();
                    this.WindowState = WindowState.Normal;
                    this.Activate();
                };

            InitializeComponent();










        }
        private void OnCanResizeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = this.ResizeMode == ResizeMode.CanResize || this.ResizeMode == ResizeMode.CanResizeWithGrip;
        }

        private void OnCanMinimizeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = this.ResizeMode != ResizeMode.NoResize;
        }

        private void OnMaximizeWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MaximizeWindow(this);
        }

        private void OnMinimizeWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MinimizeWindow(this);
        }

        private void OnRestoreWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.RestoreWindow(this);
        }
        private void OnCloseWindow(object target, ExecutedRoutedEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to exit?", "MyBot", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.Yes)
            {
                ni.Visible = false;
                ni.Icon = null;
                ni.Dispose();
                SystemCommands.CloseWindow(this);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            threadStart = new Thread(RefreshValueAndForm);
            threadStart.Start();

            DGVpotion.DataContext = Potion.list;
            DGVbuff.DataContext = Buff.list;
            DGVskill.DataContext = Skill.list;
            Bot.StaticPropertyChanged += BotStatus_Changed;

            SettingXML.LoadDefaultSetting();
            if (File.Exists(SettingXML.defaultpath))
                SettingXML.LoadSettingIni(SettingXML.defaultpath);
        }
        private static void RefreshValueAndForm()
        {
            while (true && !stopBot)
            {
                RefreshForm.Do();
                Thread.Sleep(200);
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            threadStart.Abort();
            target.Close();
            player.Close();

            //   threadStart.Join();
        }




        #region Log
        private void LogSystem_MouseUp(object sender, MouseButtonEventArgs e)
        {
            //ColorPickerDialog cPicker = new ColorPickerDialog();
            //if (cPicker.ShowDialog() == true)
            //    LogSystem.Fill = new SolidColorBrush(cPicker.SelectedColor);

        }

        private void LogHunting_MouseUp(object sender, MouseButtonEventArgs e)
        {
            //ColorPickerDialog cPicker = new ColorPickerDialog();
            //if (cPicker.ShowDialog() == true)
            //    LogHunting.Fill = new SolidColorBrush(cPicker.SelectedColor);
        }

        private void LogGather_MouseUp(object sender, MouseButtonEventArgs e)
        {
            //ColorPickerDialog cPicker = new ColorPickerDialog();
            //if (cPicker.ShowDialog() == true)
            //    LogGather.Fill = new SolidColorBrush(cPicker.SelectedColor);
        }

        private void LogUse_MouseUp(object sender, MouseButtonEventArgs e)
        {
            //ColorPickerDialog cPicker = new ColorPickerDialog();
            //if (cPicker.ShowDialog() == true)
            //    LogUse.Fill = new SolidColorBrush(cPicker.SelectedColor);
        }

        private void BTNlogclear_Click(object sender, RoutedEventArgs e)
        {
            RTBlog.Document.Blocks.Clear();
        }

        #endregion Log

        #region Script
        private void BTNscriptclear_Click(object sender, RoutedEventArgs e)
        {
            ListScript.Items.Clear();
        }

        private void BTNrecord_Click(object sender, RoutedEventArgs e)
        {
            if (BTNrecord.Content.ToString() == "Record")
            {
                BTNrecord.Content = "Stop Record";
                Properties.LastScript.Default.Fly = Value.Player.Flight.Type == 1 ? true : false;
                TimerScript.Tick += TimerScript_Tick;
                TimerScript.Interval = new TimeSpan(0, 0, 0, 0, 100);
                TimerScript.Start();
            }
            else if (BTNrecord.Content.ToString() == "Stop Record")
            {
                BTNrecord.Content = "Record";
                Properties.LastScript.Default.LastPosX = 0;
                Properties.LastScript.Default.LastPosY = 0;
                TimerScript.Stop();
            }
        }


        DispatcherTimer TimerScript = new DispatcherTimer();
        private void TimerScript_Tick(object sender, EventArgs e)
        {
            if ((int)Calc.TargetDistance(Value.Player.Position.X, Value.Player.Position.Y, Properties.LastScript.Default.LastPosX, Properties.LastScript.Default.LastPosY) > NRscriptstep.Value)
            {
                ListScript.Items.Add(Properties.ScriptSetting.Default.go + "(" + Value.Player.Position.X + ";" + Value.Player.Position.Y + ";" + Value.Player.Position.Z + ")");
                Properties.LastScript.Default.LastPosX = Value.Player.Position.X;
                Properties.LastScript.Default.LastPosY = Value.Player.Position.Y;
            }
            if ((Value.Player.Flight.Type == 1 || Value.Player.Flight.Type == 7) && Properties.LastScript.Default.Fly == false)
            {
                ListScript.Items.Add(Properties.ScriptSetting.Default.fly);
                Properties.LastScript.Default.Fly = true;
            }
            else if (!(Value.Player.Flight.Type == 1 || Value.Player.Flight.Type == 7) && Properties.LastScript.Default.Fly)
            {
                ListScript.Items.Add(Properties.ScriptSetting.Default.land);
                Properties.LastScript.Default.Fly = false;
            }
        }

        private void BTNscriptsave_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "Waypoint (*.way)|*.way";
            if (save.ShowDialog() == true)
            {
                StreamWriter writer = new StreamWriter(save.FileName);
                foreach (string s in ListScript.Items)
                {
                    if (s != "")
                        writer.WriteLine(s);
                }
                writer.Close();
            }
        }

        private void BTNscriptload_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Load");
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Waypoint (*.way)|*.way";
            if (open.ShowDialog() == true)
            {
                ListScript.Items.Clear();
                StreamReader sr = new StreamReader(open.FileName);
                while (!sr.EndOfStream)
                {
                    string text = sr.ReadLine();
                    if (text != "")
                        ListScript.Items.Add(text);
                }
                sr.Close();
            }
        }
        #endregion Script

        #region Aethertapping
        private void BTNAddAtherSafeSpot_Click(object sender, RoutedEventArgs e)
        {
            ListAetherSafeSpot.Items.Add("(" + Value.Player.Position.X + ";" + Value.Player.Position.Y + ";" + Value.Player.Position.Z + ")");
        }

        private void TSMdelete_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Asdafg");

            if (ListAetherSafeSpot.SelectedIndex != -1)
            {
                ListAetherSafeSpot.Items.RemoveAt(ListAetherSafeSpot.SelectedIndex);
            }
        }
        #endregion Aethertapping

        #region Waypoint
        private void BTNdeathbrowse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Waypoint (*.way)|*.way";
            if (open.ShowDialog() == true)
            {
                if (new Script(open.FileName).CheckScript())
                    TBdeathway.Text = open.FileName;
                else
                    MessageBox.Show("The script is incorrect");
            }
        }

        private void BTNwaybrowse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Waypoint (*.way)|*.way";
            if (open.ShowDialog() == true)
            {
                if (new Script(open.FileName).CheckScript())
                    TBactionway.Text = open.FileName;
                else
                    MessageBox.Show("The script is incorrect!");
            }
        }
        #endregion Waypoint

        #region Setting
        private void saveSettingToolStripMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Do you want save as default setting?", "MyBot", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.Yes)
            {
                if (File.Exists(SettingXML.defaultpath))
                    File.Delete(SettingXML.defaultpath);
                SettingXML.SaveSettingIni(SettingXML.defaultpath);
            }
            else
            {
                SaveFileDialog save = new SaveFileDialog();
                save.InitialDirectory = SettingXML.defaultpath;
                save.Filter = "Setting (*.xml)|*.xml";
                bool result = (bool)save.ShowDialog();
                if (result)
                    SettingXML.SaveSettingIni(save.FileName);
            }
        }

        private void loadSettingToolStripMenuItem_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Setting (*.xml)|*.xml";
            if (open.ShowDialog() == true)
            {
                SettingXML.LoadSettingIni(open.FileName);
            }
        }
        #endregion Setting

        private void CMDscriptGo_Click(object sender, RoutedEventArgs e)
        {
            ListScript.Items.Add(Properties.ScriptSetting.Default.go + "(" + Value.Player.Position.X + ";" + Value.Player.Position.Y + ";" + Value.Player.Position.Z + ")");
        }

        private void CMDscriptFly_Click(object sender, RoutedEventArgs e)
        {
            ListScript.Items.Add(Properties.ScriptSetting.Default.fly);
        }

        private void CMDscriptLand_Click(object sender, RoutedEventArgs e)
        {
            ListScript.Items.Add(Properties.ScriptSetting.Default.land);
        }

        private void CMDscriptDelete_Click(object sender, RoutedEventArgs e)
        {
            if (ListScript.SelectedIndex != 0)
            {
                ListScript.Items.RemoveAt(ListScript.SelectedIndex);
            }
        }

        private void CMDscriptClear_Click(object sender, RoutedEventArgs e)
        {
            ListScript.Items.Clear();
        }



        private void exitToolStripMenuItem_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void alwaysOnTopToolStripMenuItem_Click(object sender, RoutedEventArgs e)
        {
            this.Topmost = !this.Topmost;
        }

        private void profiToolStripMenuItem_Click(object sender, RoutedEventArgs e)
        {
            //if (profiToolStripMenuItem.Checked)
            //{
            //    player.RUN(false);
            //    profiToolStripMenuItem.Checked = false;
            //    player.Hide();
            //}
            //else
            //{
            //    profiToolStripMenuItem.Checked = true;
            //    player.Show();
            //    player.RUN(true);
            //}
        }

        private void targetInfoToolStripMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if ((bool)targetInfoToolStripMenuItem.IsChecked)
                target.Show();
            else
                target.Hide();
        }

        private void BTNStartBot_Click(object sender, RoutedEventArgs e)
        {

            if ((bool)BTNStartBot.IsChecked)
            {
                Bot.StartBot();
            }
            else
            {
                Bot.Stop();
            }
        }

        #region ContextMenu CMdatagrid
        private DataGrid GetDataGrid(object sender)
        {
            MenuItem item = sender as MenuItem;
            while (item.Parent is MenuItem)
            {
                item = (MenuItem)item.Parent;
            }
            var menu = item.Parent as ContextMenu;
            if (menu != null)
            {
                string source = (menu.PlacementTarget as DataGrid).Name;
                if (source == DGVskill.Name)
                    return DGVskill;
                else if (source == DGVbuff.Name)
                    return DGVbuff;
                else if (source == DGVpotion.Name)
                    return DGVpotion;
            }

            return null;
        }
        private void CMDpotionAdd_Click(object sender, RoutedEventArgs e)
        {

            DataGrid dgv = GetDataGrid(sender);
            if (dgv != null)
            {
                MSGBOX form;
                if (dgv == DGVskill)
                {
                    form = new MSGBOX(MSGBOX.Display.Skill);
                    form.ShowDialog();
                }
                else if (dgv == DGVbuff)
                {
                    form = new MSGBOX(MSGBOX.Display.Buff);
                    form.ShowDialog();
                }
                else if (dgv == DGVpotion)
                {
                    form = new MSGBOX(MSGBOX.Display.Potion);
                    form.ShowDialog();
                }
            }

        }

        private void CMDpotiontUp_Click(object sender, RoutedEventArgs e)
        {
            DataGrid dgv = GetDataGrid(sender);
            if (dgv != null)
            {
                if (dgv == DGVskill && (DGVskill.SelectedIndex - 1) >= 0)
                {
                    Skill.list.Move(DGVskill.SelectedIndex, DGVskill.SelectedIndex - 1);
                }
                else if (dgv == DGVbuff && (DGVbuff.SelectedIndex - 1) >= 0)
                {
                    Buff.list.Move(DGVbuff.SelectedIndex, DGVbuff.SelectedIndex - 1);
                }
                else if (dgv == DGVpotion && (DGVpotion.SelectedIndex - 1) >= 0)
                {

                    Potion.list.Move(DGVpotion.SelectedIndex, DGVpotion.SelectedIndex - 1);
                }
            }
        }

        private void CMDpotionDown_Click(object sender, RoutedEventArgs e)
        {
            DataGrid dgv = GetDataGrid(sender);
            if (dgv != null)
            {
                if (dgv == DGVskill && (DGVskill.SelectedIndex + 1) < DGVskill.Items.Count)
                {
                    Skill.list.Move(DGVskill.SelectedIndex, DGVskill.SelectedIndex + 1);
                }
                else if (dgv == DGVbuff && (DGVbuff.SelectedIndex + 1) < DGVbuff.Items.Count)
                {
                    Buff.list.Move(DGVbuff.SelectedIndex, DGVbuff.SelectedIndex + 1);
                }
                else if (dgv == DGVpotion && (DGVpotion.SelectedIndex + 1) < DGVpotion.Items.Count)
                {
                    Potion.list.Move(DGVpotion.SelectedIndex, DGVpotion.SelectedIndex + 1);
                }
            }
        }

        private void CMDpotionDelete_Click(object sender, RoutedEventArgs e)
        {
            DataGrid dgv = GetDataGrid(sender);
            if (dgv != null)
            {
                if (dgv == DGVskill)
                {
                    Skill.list.RemoveAt(DGVskill.SelectedIndex);
                }
                else if (dgv == DGVbuff)
                {
                    Buff.list.RemoveAt(DGVbuff.SelectedIndex);

                }
                else if (dgv == DGVpotion)
                {
                    Potion.list.RemoveAt(DGVpotion.SelectedIndex);
                }
            }
            Console.WriteLine();
        }

        private void CMDpotiontClear_Click(object sender, RoutedEventArgs e)
        {
            DataGrid dgv = GetDataGrid(sender);
            if (dgv != null)
            {
                if (dgv == DGVskill)
                {
                    Skill.list.Clear();
                }
                else if (dgv == DGVbuff)
                {
                    Buff.list.Clear();
                }
                else if (dgv == DGVpotion)
                {
                    Potion.list.Clear();
                }
            }
        }
        #endregion ContextMenu CMdatagrid



        private void DGautoGeneration_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            switch (e.Column.Header.ToString())
            {
                case "Date":
                    e.Column.Visibility = Visibility.Collapsed;
                    break;
            }
        }
        private void TBdeathway_TextChanged(object sender, TextChangedEventArgs e)
        {
            TBdeathway.Focus();
            TBdeathway.CaretIndex = TBdeathway.Text.Length - 1;
        }

        private void TBactionway_TextChanged(object sender, TextChangedEventArgs e)
        {
            TBactionway.Focus();
            TBactionway.CaretIndex = TBactionway.Text.Length - 1;

        }

        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowThreadProcessId(IntPtr hWnd, out uint ProcessId);

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Thread.Sleep(2000);
            Keybord ke = new Keybord(Launcher.processHandlekey);
            ke.KeybdEvent(Keybord.VKeys.KEY_1);
            //asd.Signal();
            //Thread.Sleep(1000);
            //SendMessage(processHandle, (int)Message.SYSKEYDOWN, (uint)KeyInterop.VirtualKeyFromKey(Key.LeftAlt), (uint)0);
            //SendMessage(processHandle, (int)Message.KEY_DOWN, (uint)KeyInterop.VirtualKeyFromKey(Key.D1), (uint)0);
            //Thread.Sleep(1000);
            //SendMessage(processHandle, (int)Message.KEY_UP, (uint)KeyInterop.VirtualKeyFromKey(Key.D1), (uint)0);
            //SendMessage(processHandle, (int)Message.SYSKEYUP, (uint)KeyInterop.VirtualKeyFromKey(Key.LeftAlt), (uint)0);



            //System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            //dispatcherTimer.Tick += dispatcherTimer_Tick;
            //dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            //dispatcherTimer.Start();

        }
        private void button_Click_1(object sender, RoutedEventArgs e)
        {
            asd.State = !asd.State;
            Console.WriteLine(asd.State);
            //Func<bool> a = Check;
            //Console.WriteLine(a.Invoke());
        }



        private static void BotStatus_Changed(object sender, PropertyChangedEventArgs e)
        {
            Program.bot.TBBotstatus.Text = "Bot Status: " + e.PropertyName;
            if (e.PropertyName == Enums.BotStatus.Idle.ToString())
            {
                Program.bot.BTNStartBot.Content = "Start Bot";
                Program.bot.BTNStartBot.IsChecked = false;
                Keys.GoForward2(false);
                Log.Write("BOT STOP!!", Enums.Colors.System);

            }
            else
            {
                Program.bot.BTNStartBot.Content = "Stop Bot";
                if (e.PropertyName == Enums.BotStatus.ActionWaypoint.ToString())
                {
                    Log.Write("Action Script Start!!", Enums.Colors.System);
                }

                else if (e.PropertyName == Enums.BotStatus.DeathWaypoint.ToString())
                {

                    Log.Write("Death Script Start!!", Enums.Colors.System);
                }
            }
        }

        private static void DeadStatus_Changed(object sender, PropertyChangedEventArgs e)
        {
            Console.WriteLine(e.PropertyName);
        }

        private void TB2state_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
        }
    }
}
