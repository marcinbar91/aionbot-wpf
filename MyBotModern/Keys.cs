﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MB.WPF.ProcessControl;
using System.Windows.Input;
using System.Threading;

namespace MyBotModern
{
    class Keys
    {
        static Keybord ke = new Keybord(Launcher.processHandlekey);

        public static void KeyPress(Key key)
        {
            ke.SendMessage(key);
        }

        public static void KeyPress(string str)
        {
            ke.SendMessage(str);
        }

        public static void KeyPressDown(Key key)
        {
            ke.SendMessageDown(key);
        }

        public static void KeyPressUp(Key key)
        {
            ke.SendMessageUp(key);
        }
        public static void KeyPressDown(string key)
        {
            ke.SendMessageDown(key);
        }

        public static void KeyPressUp(string key)
        {
            ke.SendMessageUp(key);
        }

        public static void GoForward2(bool move = true)
        {
            if (move && Value.Player.Movement == 0)
            {
                ke.SendMessage((Key)Program.bot.CBforward.SelectedItem);
            }
            if (move == false && Value.Player.Movement == 4)
            {
                ke.SendMessage((Key)Program.bot.CBforward.SelectedItem);

            }
        }

        private static int _lockFlag = 0; // 0 - free
        public async static void GoForward(bool move = true)
        {
            if (Interlocked.CompareExchange(ref _lockFlag, 1, 0) == 0)
            {
                Key forward = (Key)Program.bot.CBforward.SelectedItem;
                float x = Value.Player.Position.X, y = Value.Player.Position.Y;
                await Task.Delay(500);
                bool cur;
                if (Calc.TargetDistance(Value.Player.Position.X, Value.Player.Position.Y, x, y) == 0)
                    cur = false;
                else
                    cur = true;

                if (move && cur == false)
                {

                    KeyPress(forward);
                }
                else if (move == false && cur)
                    KeyPress(forward);
                Interlocked.Decrement(ref _lockFlag);
            }
        }

        public static void SelectTarget()
        {
            KeyPress((Key)Program.bot.CBselect.SelectedItem);
        }
        public static void Loot()
        {
            KeyPress((Key)Program.bot.CBloot.SelectedItem);
        }
        public static void Rest()
        {
            KeyPress((Key)Program.bot.CBrest.SelectedItem);
        }
        public static void SelectYourself()
        {
            KeyPress((Key)Program.bot.CBselectyourself.SelectedItem);
        }
        public static void Fly()
        {
            KeyPress((Key)Program.bot.CBfly.SelectedItem);
        }
        public static void Land()
        {
            KeyPress((Key)Program.bot.CBland.SelectedItem);
        }
    }
}
