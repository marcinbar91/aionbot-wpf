﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyBotModern
{
    /// <summary>
    /// Interaction logic for TextProgressbar.xaml
    /// </summary>
    public partial class TextProgressbar : UserControl
    {
        public TextProgressbar()
        {
            InitializeComponent();

        }

        #region Property


        #region Text
        [Category("Text")]
        [Description("Horizontal Alignment")]
        public HorizontalAlignment HorizontalAlignmentText
        {
            get { return txtblock.HorizontalAlignment; }
            set { txtblock.HorizontalAlignment = value; }
        }
        [Category("Text")]
        [Description("Vertical Alignment")]
        public VerticalAlignment VerticalAlignmentText
        {
            get { return txtblock.VerticalAlignment; }
            set { txtblock.VerticalAlignment = value; }
        }
        //[Category("Text")]
        //[Description("Font Size")]
        //public double FontSizeText
        //{
        //    get { return txtblock.FontSize; }
        //    set { txtblock.FontSize = value; }
        //}
        //[Category("Text")]
        //[Description("Font Color")]
        //public Brush FontColorText
        //{
        //    get { return txtblock.Foreground; }
        //    set { txtblock.Foreground = value; }
        //}
        //[Category("Text")]
        //[Description("Font Family")]
        //public FontFamily FontFamilyText
        //{
        //    get { return txtblock.FontFamily; }
        //    set { txtblock.FontFamily = value; }
        //}

        //public double Value
        //{
        //    get { return pBar.Value; }
        //    set { pBar.Value = value;  }
        //}
        //public double Maximum
        //{
        //    get { return pBar.Maximum; }
        //    set { pBar.Maximum = value;  }
        //}
        //public double Minimum
        //{
        //    get { return pBar.Minimum; }
        //    set { pBar.Minimum = value; 
        //        if (Value < pBar.Minimum) Value = minimum; 
        //    }
        //}
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(int), typeof(TextProgressbar), new PropertyMetadata(50));
        public int Value
        {
            get { return (int)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }
        public static readonly DependencyProperty MaximumProperty = DependencyProperty.Register("Maximum", typeof(int), typeof(TextProgressbar), new PropertyMetadata(100));
        public int Maximum
        {
            get { return (int)GetValue(MaximumProperty); }
            set { SetValue(MaximumProperty, value); }
        }
        public static readonly DependencyProperty MinimumProperty = DependencyProperty.Register("Minimum", typeof(int), typeof(TextProgressbar), new PropertyMetadata(0));
        public int Minimum
        {
            get { return (int)GetValue(MinimumProperty); }
            set { SetValue(MinimumProperty, value); }
        }

        public static readonly new DependencyProperty BackgroundProperty = DependencyProperty.Register("Background", typeof(Brush), typeof(TextProgressbar), new PropertyMetadata(Brushes.WhiteSmoke));
        public new Brush Background
        {
            get { return GetValue(BackgroundProperty) as Brush; }
            set { SetValue(BackgroundProperty, value); }
        }

        public static readonly new DependencyProperty ForegroundProperty = DependencyProperty.Register("Foreground", typeof(Brush), typeof(TextProgressbar), new PropertyMetadata(Brushes.Green));
        public new Brush Foreground
        {
            get { return GetValue(ForegroundProperty) as Brush; }
            set { SetValue(ForegroundProperty, value); }
        }

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(String), typeof(TextProgressbar), new FrameworkPropertyMetadata(string.Empty));

        public String Text
        {
            get { return GetValue(TextProperty).ToString(); }
            set { SetValue(TextProperty, value); }
        }
        //[Category("Text")]
        //[Description("Text in progress bar")]
        //public string Text
        //{
        //    get { return txtblock.Text; }
        //    set { txtblock.Text = value; }
        //}
        #endregion Text

        #endregion Property

        //public void Increment(float value = 1)
        //{
        //    if (Value < Maximum)
        //        Value += Value;
        //}

        //public void Decrement(float value = 1)
        //{
        //    if (Value > Minimum)
        //        Value -= Value;
        //}

    }
}
