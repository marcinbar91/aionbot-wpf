﻿using System;
using System.Drawing;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace MyBotModern
{
    class Log : Enums
    {
        public static void Write(string text, Colors color)
        {
            if (Program.bot != null)
            {
                SolidColorBrush col = new SolidColorBrush();
                switch (color)
                {
                    case Colors.System:
                        col = (SolidColorBrush)Program.bot.LogSystem.Fill;
                        break;
                    case Colors.Hunting:
                        col = (SolidColorBrush)Program.bot.LogHunting.Fill;
                        break;
                    case Colors.Gather:
                        col = (SolidColorBrush)Program.bot.LogGather.Fill;
                        break;
                    case Colors.Use:
                        col = (SolidColorBrush)Program.bot.LogUse.Fill;
                        break;
                }

                Program.bot.UIThread(delegate
                {
                    string data = DateTime.Now.ToString("HH:mm:ss tt");
                    TextRange tr = new TextRange(Program.bot.RTBlog.Document.ContentEnd, Program.bot.RTBlog.Document.ContentEnd);
                    tr.Text = data + "\t" + text + Environment.NewLine;
                    tr.ApplyPropertyValue(TextElement.ForegroundProperty, col);
                    Program.bot.RTBlog.ScrollToEnd();
                });
            }
        }
    }
}
