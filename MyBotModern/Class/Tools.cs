﻿using System;
using System.Diagnostics;
using System.Management;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Interop;

namespace MyBotModern
{
    class Tools
    {
        //public static void Sleep(uint length)
        //{
        //    DateTime start = DateTime.Now;
        //    TimeSpan restTime = new TimeSpan(10000);
        //    while (true)
        //    {
        //        System.Windows.Forms.Application.DoEvents();
        //        TimeSpan remainingTime = start.Add(TimeSpan.FromMilliseconds(length)).Subtract(DateTime.Now);
        //        if (remainingTime > restTime)
        //            System.Threading.Thread.Sleep(restTime);
        //        else
        //        {
        //            if (remainingTime.Ticks > 0)
        //                System.Threading.Thread.Sleep(remainingTime);
        //            break;
        //        }


        //    }
        //}

        #region Timer
        static Stopwatch sw;
        static DateTime startTime = DateTime.Now;

        public static void StartTimer()
        {
            sw = new Stopwatch();
            sw.Start();
            //startTime = DateTime.Now;
        }

        public static double StopTimer()
        {
            sw.Stop();
            //Console.WriteLine((DateTime.Now.Subtract(startTime).TotalSeconds).ToString());
            //return (DateTime.Now.Subtract(startTime).TotalSeconds);

            Console.WriteLine(sw.ElapsedMilliseconds);
            return sw.ElapsedMilliseconds;
        }
        #endregion Timer

        public static void CPUsage()
        {
            BackgroundWorker myWorker = new BackgroundWorker();
            myWorker.DoWork += new DoWorkEventHandler(CPUsage_DoWork);
            myWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(CPUsage_RunWorkerCompleted);
            myWorker.WorkerSupportsCancellation = true;

            if (!myWorker.IsBusy)
            {
                myWorker.RunWorkerAsync();
            }

        }

        protected static void CPUsage_DoWork(object sender, DoWorkEventArgs e)
        {
            float cpu = 100;
            //float ram = 100;
            Process process = Process.GetCurrentProcess();
            //PerformanceCounter ramCounter = new PerformanceCounter("Memory", "Available MBytes");
            PerformanceCounter cpuCounter = new PerformanceCounter("Process", "% Processor Time", process.ProcessName);
            //ramCounter.NextValue();
            cpuCounter.NextValue();
            Thread.Sleep(1000);
            cpu = (float)Math.Round(cpuCounter.NextValue() / Environment.ProcessorCount, 1);
            //ram = (float)Math.Round(ramCounter.NextValue(), 1);
            e.Result = cpu;
        }
        protected static void CPUsage_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //  Program.bot.StatCpu.Text = "CPU: " + e.Result + "%";
        }


        public static BitmapSource CaptureScreenshot()
        {
            using (var screenBmp = new Bitmap((int)SystemParameters.PrimaryScreenWidth, (int)SystemParameters.PrimaryScreenHeight, PixelFormat.Format32bppArgb))
            {
                using (var bmpGraphics = Graphics.FromImage(screenBmp))
                {
                    bmpGraphics.CopyFromScreen(0, 0, 0, 0, screenBmp.Size);
                    return Imaging.CreateBitmapSourceFromHBitmap(screenBmp.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                }
            }
        }

    }
}
