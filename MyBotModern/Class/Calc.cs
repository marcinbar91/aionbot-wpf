﻿using System;
using System.Drawing;
using System.IO;
using System.Threading;

namespace MyBotModern
{
    class Calc
    {
        public static float TargetDistance(float x1, float y1, float z1, float x2, float y2, float z2)
        {
            //     __________________________________
            //d = √ (x2-x1)^2 + (y2-y1)^2 + (z2-z1)^2
            //

            return (float)System.Math.Sqrt(Math.Pow((x2 - x1), 2) + Math.Pow((y2 - y1), 2) + Math.Pow((z2 - z1), 2));
        }
        public static float TargetDistance(float x1, float y1, float x2, float y2)
        {
            //     ______________________
            //d = √ (x2-x1)^2 + (y2-y1)^2 
            //

            return (float)System.Math.Sqrt(Math.Pow((x2 - x1), 2) + Math.Pow((y2 - y1), 2));
        }

        public static bool ImageCompareString(Bitmap firstImage, Bitmap secondImage)
        {
            MemoryStream ms = new MemoryStream();
            firstImage.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            String firstBitmap = Convert.ToBase64String(ms.ToArray());
            ms.Position = 0;
            secondImage.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            String secondBitmap = Convert.ToBase64String(ms.ToArray());

            if (firstBitmap.Equals(secondBitmap))
                return true;
            else
                return false;
        }

        public static double GetAngle(double playerx, double playery, double targetx, double targety)
        {
            double diffX = targetx - playerx;
            double diffY = targety - playery;
            double angle = Math.Atan(diffY / diffX);
            angle = (angle * 180.0) / Math.PI;
            if (diffX > 0 && diffY > 0)
                return (180 - angle);
            else if (diffX > 0 && diffY < 0)
                return (-180 - angle);
            else if (diffX < 0 && diffY < 0)
                return (0 - angle);
            else if (diffX < 0 && diffY > 0)
                return (0 - angle);

            return 0;
        }

        public static double GetAngle(double playerx, double playery, double playerz, double targetx, double targety, double targetz)
        {
            double diffX = targetx - playerx;
            double diffY = targety - playery;
            double diffZ = targetz - playerz;
            double diffXY = Math.Sqrt(Math.Pow(diffX, 2) + Math.Pow(diffY, 2));
            double angle = Math.Atan2(diffZ, diffXY);

            angle = (angle * 180.0) / Math.PI;

            return -angle;
        }

        public static long Subtract(DateTime time)
        {
            string minutes = (DateTime.Now.Subtract(time).TotalSeconds).ToString();

            //string [] array_temp = minutes.Split(',');
            //int time2 = Convert.ToInt32(array_temp[0]);

            return Convert.ToInt64(Convert.ToDouble(minutes));
        }
        public static float Percent(int cur, int max)
        {
            float x = (float)((float)cur / (float)max) * 100;
            return x;
        }

        protected T[,] ResizeArray<T>(T[,] original, int x, int y)
        {
            T[,] newArray = new T[x, y];
            int minX = Math.Min(original.GetLength(0), newArray.GetLength(0));
            int minY = Math.Min(original.GetLength(1), newArray.GetLength(1));

            for (int i = 0; i < minY; ++i)
                Array.Copy(original, i * original.GetLength(0), newArray, i * newArray.GetLength(0), minX);

            return newArray;
        }

    }
}


