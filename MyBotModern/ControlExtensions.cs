﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MyBotModern
{
    static class ControlExtensions
    {
        static public void UIThread(this Control control, Action code)
        {
            if (control.Dispatcher.CheckAccess())
            {
                control.Dispatcher.BeginInvoke(code);
                return;
            }
            code.Invoke();
        }

        static public void UIThreadInvoke(this Control control, Action code)
        {
            if (control.Dispatcher.CheckAccess())
            {
                control.Dispatcher.Invoke(code);
                return;
            }
            code.Invoke();
        }
    }
}
