﻿using System;

namespace MyBotModern
{
    public class PointXYZ
    {
        public float X;
        public float Y;
        public float Z;

        public PointXYZ(float X, float Y, float Z)
        {
            this.X = X;
            this.Y = Y;
            this.Z = Z;
        }
        public PointXYZ(string X, string Y, string Z)
        {
            this.X = float.Parse(X);
            this.Y = float.Parse(Y);
            this.Z = float.Parse(Z);
        }

        public PointXYZ()
        {
            this.X = float.MaxValue;
            this.Y = float.MaxValue;
            this.Z = float.MaxValue;
        }

        public override string ToString()
        {
            return X + ";" + Y + ";" + Z;
        }

        public float Distance(float x, float y, float z)
        {
            return (float)Math.Sqrt(Math.Pow((x - X), 2) + Math.Pow((y - Y), 2) + Math.Pow((z - Z), 2));
        }

        public float Distance()
        {
            return (float)Math.Sqrt(Math.Pow((Value.Player.Position.X - X), 2) + Math.Pow((Value.Player.Position.Y - Y), 2) + Math.Pow((Value.Player.Position.Z - Z), 2));
        }
        public float Distance(PointXYZ point)
        {
            return (float)Math.Sqrt(Math.Pow((point.X - X), 2) + Math.Pow((point.Y - Y), 2) + Math.Pow((point.Z - Z), 2));
        }

        public bool Compare(PointXYZ point)
        {
            if (X == point.X && Y == point.Y && Z == point.Z)
                return true;
            else
                return false;
        }
        public bool Compare(float x, float y, float z)
        {
            if (X == x && Y == y && Z == z)
                return true;
            else
                return false;
        }
    }
}