﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;
using System.Windows.Controls;

namespace MyBotModern
{
    public class Script
    {
        public enum Mode : byte
        {
            Death,
            Hunting,
            Aethetapping,
            Essencetapping,
            NonTarget,
        }

        #region Property
        private List<string[]> parameter = new List<string[]>();
        private List<string> command = new List<string>();
        private List<PointXYZ> scriptList = new List<PointXYZ>();
        private bool stop = true;
        private Mode mode = Mode.NonTarget;
        public List<string> Command
        {
            get { return command; }
            internal set { command = value; }
        }

        public List<string[]> Parameter
        {
            get { return parameter; }
            internal set { parameter = value; }
        }

        public bool IsLooped
        {
            get
            {
                if (scriptList.Last().Distance(scriptList.First()) < Properties.Settings.Default.MaxDistance)
                {
                    return true;
                }
                else
                    return false;
            }
        }

        public int Count
        {
            get { return Command.Count(); }
        }

        //public string[] FileScript
        //{
        //    get { return script; }
        //    set { script = value; }
        //}

        #endregion Property

        #region Constructors

        public Script(string fileScript, Mode mode = Mode.NonTarget)
        {
            //FileScript = File.ReadAllLines(fileScript);
            foreach (string line in File.ReadAllLines(fileScript))
            {
                Command.Add(GetCommand(line));
                Parameter.Add(GetParameters(line).Split(';'));
                if (Command.Last().Contains(Properties.ScriptSetting.Default.go))
                {
                    string[] par = Parameter.Last();
                    scriptList.Add(new PointXYZ(float.Parse(par[0]), float.Parse(par[1]), float.Parse(par[2])));
                }
            }
            this.mode = mode;
        }
        //public Script(string[] script)
        //{
        //    FileScript = script;
        //    foreach (string line in script)
        //    {
        //        Command.Add(GetCommand(line));
        //        Parameter.Add(GetParameters(line).Split(';'));
        //        if (Command.Last().Contains(Properties.ScriptSetting.Default.go))
        //        {
        //            string[] par = Parameter.Last();
        //            scriptList.Add(new PointXYZ(float.Parse(par[0]), float.Parse(par[1]), float.Parse(par[2])));
        //        }
        //    }
        //}


        public Script(ListBox script, Mode mode = Mode.NonTarget)
        {
            foreach (string line in script.Items)
            {
                Command.Add(string.IsNullOrEmpty(GetCommand(line)) ? Properties.ScriptSetting.Default.go : GetCommand(line));
                Parameter.Add(GetParameters(line).Split(';'));
                if (Command.Last().Contains(Properties.ScriptSetting.Default.go))
                {
                    string[] par = Parameter.Last();
                    scriptList.Add(new PointXYZ(float.Parse(par[0]), float.Parse(par[1]), float.Parse(par[2])));
                }
            }

            List<string> s = new List<string>();
            for (int i = 0; i < script.Items.Count; i++)
            {
                s.Add(Command[i] + "(" + Parameter[i] + ")");
            }

            this.mode = mode;
            //FileScript = s.ToArray();
        }
        public Script(List<PointXYZ> script, Mode mode = Mode.NonTarget)
        {
            List<string> s = new List<string>();
            scriptList = script;
            foreach (PointXYZ line in script)
            {
                Command.Add(Properties.ScriptSetting.Default.go);
                Parameter.Add(new string[] { line.X.ToString(), line.Y.ToString(), line.Z.ToString() });

                s.Add(Command.Last() + "(" + parameter.ToString() + ")");

            }
            this.mode = mode;
            //FileScript = s.ToArray();
        }
        #endregion Constructors



        private async Task Do(int i)
        {
            if (Command[i].Contains(Properties.ScriptSetting.Default.go))
            {
                await Go(float.Parse(Parameter[i][0]), float.Parse(Parameter[i][1]), float.Parse(Parameter[i][2]));
            }
            else if (Command[i].Contains(Properties.ScriptSetting.Default.delay))
            {
                await Task.Delay(int.Parse(Parameter[i][0]));
            }
            else if (Command[i].Contains(Properties.ScriptSetting.Default.stop))
            {
                Keys.GoForward(false);
                await Task.Delay(1000);
            }
            else if (Command[i].Contains(Properties.ScriptSetting.Default.press))
            {
                Keys.KeyPress(Parameter[i][0]);
                await Task.Delay(1000);
            }
            else if (Command[i].Contains(Properties.ScriptSetting.Default.buff))
            {
                await Buff.Use();
                await Task.Delay(1000);
            }
            else if (Command[i].Contains(Properties.ScriptSetting.Default.loot))
            {
                while (Value.Target.Has != 0 && stop)
                {
                    Keys.Loot();
                    await Task.Delay(500);
                }
            }
            else if (Command[i].Contains(Properties.ScriptSetting.Default.fly))
            {
                Keys.Fly();
                await Task.Delay(1000);
            }
            else if (Command[i].Contains(Properties.ScriptSetting.Default.land))
            {
                Keys.Land();
                await Task.Delay(1000);
            }
        }

        public async void Stop()
        {
            stop = false;
            await Task.Delay(500);
            Keys.GoForward(false);
        }

        public async Task Start(bool repeat = false)
        {
            do
            {
                for (int i = FindNear().Index; i < Count && stop; i++)
                {
                    if (i == Count - 1 && repeat && !IsLooped)
                    {
                        for (; i >= 0 && stop; i--)
                        {
                            await Do(i);
                        }
                    }
                    else
                    {
                        await Do(i);
                        if (i == Count - 1 && repeat)
                            i = -1;
                    }
                    await Task.Delay(100);
                }
            } while (repeat && stop);
        }

        public PointAndDistance FindNear(PointXYZ point = null)
        {
            point = point ?? new PointXYZ(Value.Player.Position.X, Value.Player.Position.Y, Value.Player.Position.Z);
            PointAndDistance p = new PointAndDistance();
            PointXYZ pointscript;
            for (int i = 0; i < Count; i++)
            {
                if (Command.Contains(Properties.ScriptSetting.Default.go))
                {
                    pointscript = new PointXYZ(Parameter[i][0], Parameter[i][1], Parameter[i][2]);
                    float dis2 = pointscript.Distance(point.X, point.Y, point.Z);
                    if (p.Distance > dis2)
                    {
                        p.Distance = dis2;
                        p.Point = pointscript;
                        p.Index = i;
                    }
                }
            }
            return p;
        }

        public Script CreatePathToSafeSpot(ListBox safelist, bool returnn)
        {
            List<PointXYZ> list = new List<PointXYZ>();
            List<PointXYZ> list2 = new List<PointXYZ>();

            PointXYZ safe = new Script(safelist).FindNear().Point;
            var nears = FindNear();
            var safes = FindNear(safe);
            PointXYZ nearScript = nears.Point;
            PointXYZ safeScript = safes.Point;

            int indexNearScript = nears.Index;
            int indexsafeScript = safes.Index;


            //Console.WriteLine("-----------------FindNear Class---------------------");
            //Console.WriteLine("safe " + safe);
            //Console.WriteLine("safeScript " + safeScript);
            //Console.WriteLine("nearScript " + nearScript);
            //Console.WriteLine("indexNearScript " + indexNearScript);
            //Console.WriteLine("indexsafeScript " + indexsafeScript);
            //Console.WriteLine("--------------------------------------------------");


            if (indexsafeScript - indexNearScript == 0)
            {
                list.Add(safe);
                return new Script(list);
            }

            for (int i = indexNearScript; i < scriptList.Count; i++)
            {
                list.Add(scriptList[i]);
                if (safeScript.Compare(scriptList[i]))
                {
                    list.Add(safe);
                    break;
                }

                if (i == scriptList.Count - 1 && !returnn)
                    i = -1;
                else if (i == scriptList.Count - 1)
                    list.Clear();
            }

            for (int i = indexNearScript; i >= 0; i--)
            {
                list2.Add(scriptList[i]);
                if (safeScript.Compare(scriptList[i]))
                {
                    list2.Add(safe);
                    break;
                }

                if (i == 0 && !returnn)
                    i = scriptList.Count;
                else if (i == 0)
                    list2.Clear();
            }

            if (!list2.Any() || (list.Count < list2.Count && list.Any()))
                return new Script(list);
            else
                return new Script(list2);
        }

        private int IndexOf(List<PointXYZ> script, PointXYZ safeScript)
        {
            foreach (var item in script)
            {
                if (item.X == safeScript.X && item.Y == safeScript.Y && item.Z == safeScript.Z)
                {
                    return script.IndexOf(item);
                }
            }
            return -1;
        }

        public bool CheckScript()
        {
            for (int i = 0; i < Count; i++)
            {
                if (Command[i].Contains(Properties.ScriptSetting.Default.go)) { }
                else if (Command[i].Contains(Properties.ScriptSetting.Default.delay)) { }
                else if (Command[i].Contains(Properties.ScriptSetting.Default.stop)) { }
                else if (Command[i].Contains(Properties.ScriptSetting.Default.press)) { }
                else if (Command[i].Contains(Properties.ScriptSetting.Default.buff)) { }
                else if (Command[i].Contains(Properties.ScriptSetting.Default.loot)) { }
                else if (Command[i].Contains(Properties.ScriptSetting.Default.fly)) { }
                else if (Command[i].Contains(Properties.ScriptSetting.Default.land)) { }
                else
                    return false;
            }
            return true;
        }

        private string GetParameters(string s)
        {
            return Regex.Match(s, @"(?<=\()(.*?)(?=\))").ToString();
        }

        private string GetCommand(string s)
        {
            return Regex.Match(s, @"[^(\)]*").ToString().ToLower();
        }



        #region hunting
        private async Task Go(float xCoord, float yCoord, float zCoord)
        {
            double dis = Calc.TargetDistance(Value.Player.Position.X, Value.Player.Position.Y, xCoord, yCoord);
            while (dis > 5 && stop)
            {

                if (mode == Mode.Hunting)
                {
                    Keys.SelectTarget();
                    await Task.Delay(100);
                    if (Value.Target.HP.Cur == 0 && stop)
                    {

                        await Buff.Use();
                        await Potion.Rest();
                        await Potion.Use();
                    }
                    else
                    {
                        await Kill();
                    }
                }
                Keys.GoForward();
                dis = Calc.TargetDistance(Value.Player.Position.X, Value.Player.Position.Y, xCoord, yCoord);
                Value.Player.Camera.Set(xCoord, yCoord);
                await Task.Delay(100);
            }
        }

        private async Task Kill()
        {
            Log.Write("Killing " + Value.Target.Name, Enums.Colors.Hunting);
            Keys.GoForward(false);
            while (Value.Target.HP.Cur != 0 && stop)
            {
                await Buff.Use();
                await Potion.Use();
                await Skill.Use2();
                await Task.Delay(100);
            }
            Log.Write("Killed " + Value.Target.Name, Enums.Colors.Hunting);
            await GetLoot();
        }

        private async Task GetLoot()
        {
            if (Program.bot.CBHuntingGetLoot.IsChecked.Value)
            {
                Log.Write("Get Loot from " + Value.Target.Name, Enums.Colors.Gather);
                while (Value.Target.Has == 1 && Value.Target.HP.Cur == 0 && stop)
                {
                    Keys.Loot();
                    await Task.Delay(100);
                }
            }
        }
        #endregion hunting

        public class PointAndDistance
        {
            PointXYZ point = new PointXYZ();
            float distance = float.MaxValue;
            int index = -1;
            public PointXYZ Point
            {
                get { return point; }
                set { point = value; }
            }

            public float Distance
            {
                get { return distance; }
                set { distance = value; }
            }

            public int Index
            {
                get { return index; }
                set { index = value; }
            }
        }
    }
}
