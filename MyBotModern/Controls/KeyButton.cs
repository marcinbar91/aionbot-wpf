﻿using MB.GetKey;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace MyBotModern
{
    class KeyButton : Button
    {

        private List<Key> _pressedKeys = new List<Key>();
        private bool multi = true;
        private string lbtxt = "Wait on key down";
        private Key[] keyss = null;
        private Window form;
        private bool tooltip = true;

        public bool MultiKeys
        {
            get { return multi; }
            set { multi = value; }
        }
        [Description("Show Tooltip if Text size is bigger from button")]
        public bool ToolTip2
        {
            get { return tooltip; }
            set { tooltip = value; }
        }

        public string SetText
        {
            get { return lbtxt; }
            set { lbtxt = value; }
        }

        private Key[] SetKey
        {
            get { return keyss; }
            set
            {
                keyss = value;

                Content = KeysToString(keyss);

            }
        }

        public KeyButton()
        {
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

        }

        #region Form
        Label LBLtext = new Label();
        protected override void OnClick()
        {
            base.OnClick();
            form = new Window();
            InitializeForm(form);
            form.ShowDialog();
        }

        public void InitializeForm(Window form)
        {
            form.ShowInTaskbar = false;
            form.Activated += new EventHandler(Form1_Activated);
            // form.Closing += new ClosingEventHandler(Form1_FormClosing);
            form.KeyDown += new KeyEventHandler(Form1_KeyDown);
            form.KeyUp += new KeyEventHandler(Form1_KeyUp);
            form.Height = 100;
            form.Width = 100;
            form.WindowStyle = WindowStyle.None;
            form.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            // LBLtext.Location = new Point(12, 9);
            LBLtext.Name = "LBLtext";

            LBLtext.Content = lbtxt;
            LBLtext.HorizontalAlignment = HorizontalAlignment.Center;
            LBLtext.VerticalAlignment = VerticalAlignment.Center;
            //LBLtext.Width = 100;
            //LBLtext.Height = 100;


            form.Content = LBLtext;


        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (_pressedKeys.Contains(e.Key))
                return;
            if (!multi)
                _pressedKeys.Clear();
            _pressedKeys.Add(e.Key);
            e.Handled = true;

            LBLtext.Content = KeysToString(_pressedKeys.ToArray());

            keyss = _pressedKeys.ToArray();


        }


        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            _pressedKeys.Remove(e.Key);
            e.Handled = true;
            if (_pressedKeys.Count == 0)
            {
                Content = LBLtext.Content;
                form.Close();
                form = null;
            }
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            LBLtext.Content = SetText;
        }


        private void Form1_FormClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _pressedKeys.Clear();
        }
        #endregion Form


        #region public methods
        public Key[] GetKeys()
        {
            return keyss;
        }
        public Key GetKeys(int nr = 0)
        {
            return keyss[nr];
        }

        public int[] GetKeysValue()
        {
            int[] tab = new int[keyss.Length];
            for (int i = 0; i < _pressedKeys.Count; i++)
            {
                tab[i] = keyss[i].ToString().ConvertToInt();
            }
            return tab;
        }

        public string KeysToString(Key[] key)
        {
            StringBuilder b = new StringBuilder();
            foreach (Key keyy in key)
            {
                b.Append(keyy.ToString());
                b.Append("+");
            }
            if (b[b.Length - 1] == '+')
                b.Length--;

            return b.ToString();
        }

        public Key[] StringToKeys(string key)
        {
            string[] split = key.Split('+');
            List<Key> k = new List<Key>();
            foreach (string s in split)
            {
                k.Add((Key)(int)Enum.Parse(typeof(Key), s));
            }

            return k.ToArray();
        }

        public void SetKeys(Key[] key)
        {
            SetKey = key;
        }
        public void SetKeys(Key key)
        {
            SetKey = new Key[] { key };
        }
        #endregion public methods
    }
}



namespace MB.GetKey
{
    public static class GetKey
    {
        public static string ConvertToString(this int value)
        {
            return Enum.GetName(typeof(Key), value);
        }

        public static int ConvertToInt(this string str)
        {
            return (int)Enum.Parse(typeof(Key), str);
        }
        public static Key ConvertToKey(this string str)
        {
            return (Key)(int)Enum.Parse(typeof(Key), str);
        }
    }
}