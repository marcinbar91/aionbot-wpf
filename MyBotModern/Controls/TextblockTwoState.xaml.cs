﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyBotModern
{
    public partial class TextblockTwoState : UserControl
    {
        bool state;
        public TextblockTwoState()
        {
            InitializeComponent();
            //     textBlock.ClearValue(ForegroundProperty);
            State = false;

        }

        public bool State
        {
            get { return state; }
            set
            {
                if (value)
                    textBlock.Foreground = TrueColor;
                else
                    textBlock.Foreground = FalseColor;
                state = value;
            }
        }

        public static readonly DependencyProperty TrueColorProperty = DependencyProperty.Register("Color if True", typeof(Brush), typeof(TextblockTwoState), new PropertyMetadata(Brushes.Green));
        public Brush TrueColor
        {
            get { return GetValue(TrueColorProperty) as Brush; }
            set { SetValue(TrueColorProperty, value); }
        }

        public static readonly DependencyProperty FalseColorProperty = DependencyProperty.Register("Color if False", typeof(Brush), typeof(TextblockTwoState), new PropertyMetadata(Brushes.Red, OnCaptionPropertyChanged));
        public Brush FalseColor
        {
            get { return GetValue(FalseColorProperty) as Brush; }
            set { SetValue(FalseColorProperty, value); }
        }

        private static void OnCaptionPropertyChanged(DependencyObject dependencyObject,
               DependencyPropertyChangedEventArgs e)
        {
            TextblockTwoState myUserControl = dependencyObject as TextblockTwoState;
            myUserControl.OnPropertyChanged(FalseColorProperty.Name);
            myUserControl.OnCaptionPropertyChanged(e);
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        private void OnCaptionPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (State)
                textBlock.Foreground = TrueColor;
            else
                textBlock.Foreground = FalseColor;
        }

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(String), typeof(TextblockTwoState), new FrameworkPropertyMetadata("TextBlock"));
        public String Text
        {
            get { return GetValue(TextProperty).ToString(); }
            set
            {
                if (value != Text)
                    SetValue(TextProperty, value);
            }
        }
        public static readonly DependencyProperty DelaySignalProperty = DependencyProperty.Register("DelaySignal", typeof(int), typeof(TextblockTwoState), new PropertyMetadata(300));
        public int DelaySignal
        {

            get { return (int)GetValue(DelaySignalProperty); }
            set { SetValue(DelaySignalProperty, value); }
        }

        public static readonly DependencyProperty NumberSignalProperty = DependencyProperty.Register("Number of signals", typeof(int), typeof(TextblockTwoState), new PropertyMetadata(5));
        public int NumberSignal
        {

            get { return (int)GetValue(NumberSignalProperty); }
            set { SetValue(NumberSignalProperty, value); }
        }


        private int _lockFlag = 0; // 0 - free
        public async void Signal()
        {
            if (Interlocked.CompareExchange(ref _lockFlag, 1, 0) == 0)
            {
                for (int i = 0; i < NumberSignal && !State; i++)
                {
                    textBlock.Foreground = textBlock.Foreground == Foreground ? FalseColor : Foreground;
                    await Task.Delay(DelaySignal);
                }
                textBlock.Foreground = State == true ? TrueColor : FalseColor;
                Interlocked.Decrement(ref _lockFlag);
            }
        }
    }
}