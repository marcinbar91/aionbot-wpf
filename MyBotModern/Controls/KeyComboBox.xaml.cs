﻿using System;
using System.Collections;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace MyBotModern
{
    public partial class KeyComboBox : UserControl
    {
        public KeyComboBox()
        {
            InitializeComponent();
            Binding selectedItemBinding = new Binding("SelectedItem");
            selectedItemBinding.Source = this;
            selectedItemBinding.Mode = BindingMode.TwoWay;
            comboBox.SetBinding(ComboBox.SelectedItemProperty, selectedItemBinding);
            comboBox.ItemsSource = Enum.GetValues(typeof(Key)).Cast<Key>();
        }

        public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(KeyComboBox), new PropertyMetadata(null));
        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set
            {
                SetValue(ItemsSourceProperty, value);
                comboBox.ItemsSource = value;
            }
        }



        public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register("SelectedItem", typeof(object), typeof(KeyComboBox), new PropertyMetadata(null));
        public object SelectedItem
        {
            get { return GetValue(SelectedItemProperty); }
            set
            {
                SetValue(SelectedItemProperty, value);
                comboBox.SelectedItem = SelectedItem;

                for (int i = 0; i < comboBox.Items.Count; i++)
                {
                    if (SelectedItem.ToString() == comboBox.Items[i].ToString())
                    {
                        comboBox.SelectedIndex = i;
                        break;
                    }
                }
            }
        }
        public static readonly DependencyProperty SelectedIndexProperty = DependencyProperty.Register("SelectedIndex", typeof(int), typeof(KeyComboBox), new PropertyMetadata(null));
        public int SelectedIndex
        {
            get { return (int)GetValue(SelectedIndexProperty); }
            set
            {
                SetValue(SelectedIndexProperty, value);
                comboBox.SelectedIndex = SelectedIndex;
            }
        }


        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Console.Write("Index " + comboBox.SelectedIndex);
            //Console.WriteLine(" Name " + comboBox.SelectedItem);

        }

        private void comboBox_KeyDown(object sender, KeyEventArgs e)
        {
            SelectedItem = e.Key;
            comboBox.IsDropDownOpen = false;
        }
    }
}
