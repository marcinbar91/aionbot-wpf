﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyBotModern
{
    /// <summary>
    /// Logika interakcji dla klasy NumericUpDown.xaml
    /// </summary>
    public partial class NumericUpDown : UserControl
    {

        public NumericUpDown()
        {
            InitializeComponent();
            txtNum.Text = _numValue.ToString();
        }
        private int minimum = 0;
        private int maximum = 100;
        private int _numValue = 0;

        public int Value
        {
            get { return _numValue; }
            set
            {
                _numValue = value;
                txtNum.Text = value.ToString();
            }
        }

        public int Maximum
        {
            get { return maximum; }
            set
            {
                maximum = value;
                if (value > Value)
                    Value = value;
            }
        }
        public int Minimum
        {
            get { return minimum; }
            set
            {
                minimum = value;
                if (value < Value)
                    Value = value;
            }
        }

        private void cmdUp_Click(object sender, RoutedEventArgs e)
        {
            if (!(Value >= Maximum))
                Value++;
        }

        private void cmdDown_Click(object sender, RoutedEventArgs e)
        {
            if (!(Value <= Minimum))
                Value--;
        }

        private void txtNum_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtNum == null)
                return;

            if (int.TryParse(txtNum.Text, out _numValue))
                txtNum.Text = _numValue.ToString();

        }

        private void OnTargetUpdated(object sender, DataTransferEventArgs e)
        {
        }
    }
}
