﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyBotModern
{
    public partial class TextProgressbar : UserControl
    {
        public TextProgressbar()
        {
            InitializeComponent();

        }

        #region Property


        #region Text

        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(int), typeof(TextProgressbar), new PropertyMetadata(50));
        public int Value
        {
            get { return (int)GetValue(ValueProperty); }
            set
            {
                if (value != Value)
                    SetValue(ValueProperty, value);
            }
        }
        public static readonly DependencyProperty MaximumProperty = DependencyProperty.Register("Maximum", typeof(int), typeof(TextProgressbar), new PropertyMetadata(100));
        public int Maximum
        {
            get { return (int)GetValue(MaximumProperty); }
            set
            {
                if (value != Maximum)
                    SetValue(MaximumProperty, value);
            }
        }
        public static readonly DependencyProperty MinimumProperty = DependencyProperty.Register("Minimum", typeof(int), typeof(TextProgressbar), new PropertyMetadata(0));
        public int Minimum
        {
            get { return (int)GetValue(MinimumProperty); }
            set
            {
                if (value != Minimum)
                    SetValue(MinimumProperty, value);
            }
        }

        public static readonly new DependencyProperty BackgroundProperty = DependencyProperty.Register("Background", typeof(Brush), typeof(TextProgressbar), new PropertyMetadata(Brushes.Gray));
        public new Brush Background
        {
            get { return GetValue(BackgroundProperty) as Brush; }
            set { SetValue(BackgroundProperty, value); }
        }

        public static readonly new DependencyProperty ForegroundProperty = DependencyProperty.Register("Foreground", typeof(Brush), typeof(TextProgressbar), new PropertyMetadata(Brushes.Green));
        public new Brush Foreground
        {
            get { return GetValue(ForegroundProperty) as Brush; }
            set { SetValue(ForegroundProperty, value); }
        }

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(String), typeof(TextProgressbar), new FrameworkPropertyMetadata(string.Empty));
        public String Text
        {
            get { return GetValue(TextProperty).ToString(); }
            set
            {
                if (value != Text)
                    SetValue(TextProperty, value);
            }
        }
        #endregion Text
        #endregion Property
    }
}
