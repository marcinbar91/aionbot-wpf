﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBotModern
{
    public class Offsets
    {
        public enum Player : int
        {
            ID = Name - 0x4,
            Name = 0x10AE6EC,
            //TargetID = 0xE6DA54,
            Class = Level + 0x9C,
            //Loggin = 0xEB01F8,
            Level = CurHP - 0x28,
            Movement = 0x10ADE9C,                                              // 0 or 4             Name - Movement = 838       CameraX - Movement = 1C
            Legion = 0xE1EEF8,
            LegionAP = 0xE1FD98,
            MaxHP = CurHP - 0x4,
            CurHP = 0x10B8A20,
            MaxMP = CurMP - 0x4,
            CurMP = CurHP + 0x8,
            MaxDP = CurDP - 0x2,
            CurDP = CurHP + 0xE,
            MaxEXP = CurEXP - 0x10,
            CurEXP = CurHP - 0x10,
            RecEXP = MaxEXP + 0x8,
            CurCube = MaxCube + 0x4,
            MaxCube = CurHP + 0x64,
            PosX = PosY + 0x4,
            PosY = Name - 0x42C,
            PosZ = PosY + 0x8,
            CameraX = 0x10ADEB8,
            CameraY = CameraX - 0x8,
            PetAlive = 0x10FCFA4,                                            // 0 or 1            
            PetName = PetAlive + 0xE4,
            SummonAlive = 0x10FCFAC,                                         // 0 or 1 3 when wait to off
            SkillbarNumber = 0x10A0128,                                      // number -1

            FlightType = FlightCur + 0x4,                                    //1=Fly,0=NoFly,3=flyglide,2=NoFlyglide
            FlightMax = FlightCur - 0x4,
            FlightCur = CurHP + 0x14,
            FlightCooldown = FlightCur + 0x108,

            Dead = 0x1126780,                                               // 0 or 1 if died  
            //DeadNotification = Dead - 0x4,

            Stance = Target.HasTarget + 0x8                                 // 0xAE01CC,
        }
        public enum Stats : int
        {
            Power = PhyDef - 0x60,
            Health = Power + 0x2,
            Agility = Power + 0x4,
            Accuracy = Power + 0x6,
            Knowledge = Power + 0x8,
            Will = Power + 0xA,
            //1
            AttackMain = PhyDef + 0x4,
            AttackMain2 = PhyDef - 0x6,
            AttackOff = PhyDef + 0x6,
            AttackOff2 = PhyDef - 0x4,
            AccMain = PhyDef + 0x20,
            AccOff = PhyDef + 0x22,
            CritStrikeMain = PhyDef + 0x1C,
            CritStrikeOff = PhyDef + 0x1E,
            AttackSpeed = PhyDef + 0x14,
            Speed = PhyDef + 0xF4,
            //2
            PhyDef = Player.CurHP + 0x20,
            Block = PhyDef + 0x1A,
            Parry = PhyDef + 0x18,
            Evansion = PhyDef + 0x16,
            CritStrikeResist = PhyDef + 0x3A,
            CritStrikeFort = PhyDef + 0x3E,
            //3
            MagBoost = PhyDef + 0x34,
            MagAcc = PhyDef + 0x26,
            CritSpell = PhyDef + 0x28,
            HealingBoost = PhyDef + 0x38,
            CastSpeed = PhyDef + 0x2C,
            //4
            MagicDef = PhyDef + 0x58,
            MagicSupp = PhyDef + 0x36,
            MagicResist = PhyDef + 0x0C,
            CritSpellResist = PhyDef + 0x3C,
            SpellFortitude = PhyDef + 0x36,
            FireResist = WaterResist + 0x6,
            WindResist = WaterResist + 0x2,
            WaterResist = Power + 0xC,
            EarthResist = WaterResist + 0x4,

            //Abyss\Honor Point
            TotalAp = 0x10B60A0,
            TotalHp = TotalAp + 0x8,
            TotalEly = TotalAp + 0x24,
            RankNumber = TotalAp + 0x10,
            Rank = TotalAp + 0xC,
            TodayEly = TotalAp + 0x18,
            ThisWeekEly = TotalAp + 0x1C,
            LastWeekEly = TotalAp + 0x20,
            TodayAp = TotalAp + 0x38,
            ThisWeekAp = TotalAp + 0x40,
            LastWeekAp = TotalAp + 0x48,
            TodayHp = TotalAp + 0x28,
            ThisWeekHp = TotalAp + 0x2C,
            LastWeekHp = TotalAp + 0x30,
        }

        public enum Statnew : int
        {
            MonsterDef = 0xF531C8,

        }

        public enum Target : int
        {
            Pointer = HasTarget - 0x8,
            HasTarget = 0xCA8674,                                           //1=Yes, 0=No    
            Select = 0xEB01F8,                                              //65535 none
            Type = 0x18,
            Type2 = 0x1d8,
            ID = 0x28,
            PosX = 0x38,
            PosY = 0x34,
            PosZ = 0x3C,
            Entity = 0x27C,                                                 //Pointer

            TargetTargetID = 0x338,
            Level = 0x3A,
            Name = 0x3E,

            MaxMP = CurMP + 0x4,
            CurMP = CurHP + 0x4,
            MaxHP = CurHP - 0x4,
            CurHP = 0x13B4,
            Class = 0x224,
            Stance = Class + 0x8C,                       // 0x2B0 sit=3 weapon=1
            Movement = Stance + 0x4,                    // 0 stay 1 - walk 2 - run

        }

        public enum Craft : int
        {
            Crafting = 0xE292B8,                            //1/0
            Success = Crafting - 0x10,                      //0-100
            Fail = Crafting - 0xC,                          //0-100
            Left = Crafting - 0x4C,
        }


        public enum Buff : int
        {
            Pointer = 0x010AC3AC,
            Count = 0x5E8,
            Offset1 = 0x480,      //next +0x4     // null == 51005
            Offset2 = 0x3B4,
        }
        public enum Debuff : int
        {
            Pointer = 0xEC8B6C,
            Count = 584,
            //Offset1 = 0x480,      //next +0x4
            //Offset2 = 0x3D0,
        }

        public enum Skillbar : int
        {
            FirstIteam = 0x10A7984,                     //First is skillbar no. 1
            NextBar = 0x3C0,
            NextSkill = 0x14,
            NextRow = 0xF0,
        }

        public enum Party : int
        {
            Count = 0xE70B80,
            Pointer = Count - 0x4,
            Offset1 = 0x0,                      // +0x4 next
            Offset2 = 0x8,

            MaxHP = CurHP - 0x4,
            CurHP = 0x14,
            MaxMP = CurMP - 0x4,
            CurMP = 0xC,

            FlightMax = FlightCur - 0x4,
            FlightCur = 0x1c,

            Name = 0x3f
        }

    }
}
