﻿using MB.WPF.ProcessControl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBotModern
{
    public class Value
    {
        static Memory memory = new Memory(Launcher.aionProcess, "Game.dll");

        public class Player
        {
            //public static int Loggin
            //{
            //    get { return memory.Read((int)Offsets.Player.Loggin, 16).ConvertToInt16(); }
            //}

            public static string Name
            {
                get { return memory.Read((int)Offsets.Player.Name, 128).ConvertToString(); }
            }

            public static int Class
            {
                get { return memory.Read((int)Offsets.Player.Class).ConvertToInt32(); }
            }

            static int lvl = memory.Read((int)Offsets.Player.Level, 16).ConvertToInt16();
            public static int Level
            {

                get
                {
                    Level = memory.Read((int)Offsets.Player.Level, 16).ConvertToInt16();
                    return lvl;
                }
                private set
                {

                    if (lvl != value)
                    {
                        Log.Write("You have raised to " + lvl + " Level", Enums.Colors.Gather);
                        lvl = value; ;

                    }
                }
            }

            public static int Movement
            {
                get { return memory.Read((int)Offsets.Player.Movement).ConvertToInt32(); }
            }

            public static int SkillbarNumber
            {
                get { return memory.Read((int)Offsets.Player.SkillbarNumber, 16).ConvertToInt16(); }

                set
                {
                    if (value == 0)
                        value = 10;

                    //while (SkillbarNumber != value - 1)
                    //{
                    memory.Write((int)Offsets.Player.SkillbarNumber, value - 1);
                    //}
                }
            }
            static bool dead = false;
            public static bool Dead
            {
                get
                {
                    bool temp = Convert.ToBoolean(memory.Read((int)Offsets.Player.Dead, 16).ConvertToInt16());
                    if (!temp && temp != dead)
                        Log.Write("Character died", Enums.Colors.Hunting);
                    dead = temp;
                    return dead;
                }
            }
            public static event EventHandler<PropertyChangedEventArgs> DeadChanged;

            private static void NotifyDeadChanged(string propertyName)
            {
                DeadChanged?.Invoke(null, new PropertyChangedEventArgs(propertyName));
            }
            public class Position
            {
                public static float X
                {
                    get { return memory.Read((int)Offsets.Player.PosX).ConvertToSingle(); }

                }

                public static float Y
                {
                    get { return memory.Read((int)Offsets.Player.PosY).ConvertToSingle(); }
                }

                public static float Z
                {
                    get { return memory.Read((int)Offsets.Player.PosZ).ConvertToSingle(); }
                }
            }
            public class Camera
            {
                public static float X
                {
                    get { return memory.Read((int)Offsets.Player.CameraX).ConvertToSingle(); }
                    set { memory.Write((int)Offsets.Player.CameraX, value); }
                }

                public static float Y
                {
                    get { return memory.Read((int)Offsets.Player.CameraY).ConvertToSingle(); }

                    set { memory.Write((int)Offsets.Player.CameraY, value); }
                }
                public static void Set(float xCoord, float yCoord)
                {
                    float angleX = (float)Math.Round(Calc.GetAngle(Position.X, Position.Y, xCoord, yCoord), 3);
                    if (Math.Abs(angleX - X) > 10)
                        X = angleX;
                    if (Math.Round(Y) != 20)
                        Y = 20;
                }

                public static void Set(float xCoord, float yCoord, float zCoord)
                {

                    float angleY = (float)Math.Round(Calc.GetAngle(Position.X, Position.Y, Position.Z, xCoord, yCoord, zCoord), 3);
                    float angleX = (float)Math.Round(Calc.GetAngle(Position.X, Position.Y, xCoord, yCoord), 3);
                    if (Math.Abs(angleX - X) > 10)
                        X = angleX;
                    if (Math.Abs(angleY - Y) > 10 && Flight.Type != 0)
                        Y = angleY;
                    else if (Math.Round(Y) != 20)
                        Y = 20;
                }
            }
            public class Pet
            {
                public static int Alive
                {
                    get { return memory.Read((int)Offsets.Player.PetAlive, 1).ConvertToByte(); }
                }

                public static string Name
                {
                    get { return memory.Read((int)Offsets.Player.PetName).ConvertToString(); }
                }
            }
            public class Legion
            {

                public static string Name
                {
                    get { return memory.Read((int)Offsets.Player.Legion, 128).ConvertToString(); }
                }

                public static int AP
                {
                    get { return memory.Read((int)Offsets.Player.LegionAP).ConvertToInt32(); }
                }
            }
            public class HP
            {

                public static int Max
                {
                    get
                    { return memory.Read((int)Offsets.Player.MaxHP).ConvertToInt32(); }
                }
                public static int Cur
                {
                    get { return memory.Read((int)Offsets.Player.CurHP).ConvertToInt32(); }
                }
            }
            public class MP
            {
                public static int Max
                {
                    get { return memory.Read((int)Offsets.Player.MaxMP).ConvertToInt32(); }
                }
                public static int Cur
                {
                    get { return memory.Read((int)Offsets.Player.CurMP).ConvertToInt32(); }
                }
            }
            public class DP
            {
                public static int Max
                {
                    get { return memory.Read((int)Offsets.Player.MaxDP, 16).ConvertToInt16(); }
                }

                public static int Cur
                {
                    get { return memory.Read((int)Offsets.Player.CurDP, 16).ConvertToInt16(); }
                }
            }
            public class Flight
            {
                public static int Type
                {
                    get { return memory.Read((int)Offsets.Player.FlightType, 1).ConvertToByte(); }
                }
                public static int Max
                {
                    get { return memory.Read((int)Offsets.Player.FlightMax).ConvertToInt32(); }
                }

                public static int Cur
                {
                    get { return memory.Read((int)Offsets.Player.FlightCur).ConvertToInt32(); }
                }

                public static int Cooldown
                {
                    get { return memory.Read((int)Offsets.Player.FlightCooldown).ConvertToInt32(); }
                }
            }
            public class EXP
            {
                public static int Max
                {
                    get { return memory.Read((int)Offsets.Player.MaxEXP).ConvertToInt32(); }
                }
                static int expcur = memory.Read((int)Offsets.Player.CurEXP).ConvertToInt32();
                public static int Cur
                {
                    get
                    {
                        Cur = memory.Read((int)Offsets.Player.CurEXP).ConvertToInt32();
                        return expcur;
                    }
                    private set
                    {
                        if (expcur != value)
                        {
                            Log.Write("You have gained " + (value - expcur) + " XP", Enums.Colors.Gather);
                            expcur = value;

                        }
                    }
                }
                public static int Rec
                {
                    get { return memory.Read((int)Offsets.Player.RecEXP).ConvertToInt32(); }
                }
            }
            public class Cube
            {
                public static int Max
                {
                    get { return memory.Read((int)Offsets.Player.MaxCube).ConvertToInt32(); }
                }

                public static int Cur
                {
                    get { return memory.Read((int)Offsets.Player.CurCube).ConvertToInt32(); }
                }
            }
            public class Crafting
            {
                public static int Is
                {
                    get { return memory.Read((int)Offsets.Craft.Crafting, 16).ConvertToInt16(); }
                }

                public static int CraftSuccess
                {
                    get { return memory.Read((int)Offsets.Craft.Success, 16).ConvertToInt16(); }
                }
                public static int CraftFail
                {
                    get { return memory.Read((int)Offsets.Craft.Fail, 16).ConvertToInt16(); }
                }
                public static int CraftLeft
                {
                    get { return memory.Read((int)Offsets.Craft.Left, 16).ConvertToInt16(); }
                }
            }
        }
        public class Target
        {

            public static int Has
            {
                get { return memory.Read((int)Offsets.Target.HasTarget, 1).ConvertToByte(); }
            }

            public static string Name
            {
                get
                {
                    if (Has == 1)
                        return memory.Read(new int[] { (int)Offsets.Target.Pointer, (int)Offsets.Target.Entity, (int)Offsets.Target.Name }, 128).ConvertToString();
                    return null;
                }

            }

            public static int Lvl
            {
                get
                {
                    if (Has == 1)
                        return memory.Read(new int[] { (int)Offsets.Target.Pointer, (int)Offsets.Target.Entity, (int)Offsets.Target.Level }, 16).ConvertToInt16();
                    return 0;
                }
            }

            public static int Distance
            {
                get
                {
                    if (Has == 1)
                        return (int)(System.Math.Sqrt(Math.Pow((Target.Position.X - (Player.Position.X - 1.74)), 2) + Math.Pow((Target.Position.Y - (Player.Position.Y - 1.5)), 2) + Math.Pow((Target.Position.Z - (Player.Position.Z - 1)), 2)));
                    //    return int.Parse(memory.Read(new int[] { (int)Offsets.Target.DistanceBase, (int)Offsets.Target.DistanceOff1, (int)Offsets.Target.DistanceOff2, (int)Offsets.Target.DistanceOff3 }).ConvertToRemovedCharactersString());
                    return 0;
                }
            }

            public class Position
            {
                public static float X
                {
                    get
                    {
                        if (Has == 1)
                            return memory.Read(new int[] { (int)Offsets.Target.Pointer, (int)Offsets.Target.PosX }).ConvertToSingle();
                        return 0;
                    }
                }

                public static float Y
                {
                    get
                    {
                        if (Has == 1)
                            return memory.Read(new int[] { (int)Offsets.Target.Pointer, (int)Offsets.Target.PosY }).ConvertToSingle();
                        return 0;
                    }
                }

                public static float Z
                {
                    get
                    {
                        if (Has == 1)
                            return memory.Read(new int[] { (int)Offsets.Target.Pointer, (int)Offsets.Target.PosZ }).ConvertToSingle();
                        return 0;
                    }
                }
            }

            public class HP
            {
                public static int Cur
                {
                    get
                    {
                        if (Has == 1)
                            return memory.Read(new int[] { (int)Offsets.Target.Pointer, (int)Offsets.Target.Entity, (int)Offsets.Target.CurHP }).ConvertToInt32();
                        return 0;
                    }
                }

                public static int Max
                {
                    get
                    {
                        if (Has == 1)
                            return memory.Read(new int[] { (int)Offsets.Target.Pointer, (int)Offsets.Target.Entity, (int)Offsets.Target.MaxHP }).ConvertToInt32();
                        return 0;
                    }
                }
            }
            public class MP
            {
                public static int Cur
                {
                    get
                    {
                        if (Has == 1)
                            return memory.Read(new int[] { (int)Offsets.Target.Pointer, (int)Offsets.Target.Entity, (int)Offsets.Target.CurMP }).ConvertToInt32();
                        return 0;
                    }
                }

                public static int Max
                {
                    get
                    {
                        if (Has == 1)
                            return memory.Read(new int[] { (int)Offsets.Target.Pointer, (int)Offsets.Target.Entity, (int)Offsets.Target.MaxMP }).ConvertToInt32();
                        return 0;
                    }
                }
            }

        }
        public class Stat
        {
            public static int Power
            {
                get { return memory.Read((int)Offsets.Stats.Power, 16).ConvertToInt16(); }
            }

            public static int Health
            {
                get { return memory.Read((int)Offsets.Stats.Health, 16).ConvertToInt16(); }
            }

            public static int Agility
            {
                get { return memory.Read((int)Offsets.Stats.Agility, 16).ConvertToInt16(); }
            }

            public static int Accuracy
            {
                get { return memory.Read((int)Offsets.Stats.Accuracy, 16).ConvertToInt16(); }
            }

            public static int Knowledge
            {
                get { return memory.Read((int)Offsets.Stats.Knowledge, 16).ConvertToInt16(); }
            }

            public static int Will
            {
                get { return memory.Read((int)Offsets.Stats.Will, 16).ConvertToInt16(); }
            }

            public static int AttackMain
            {
                get
                {
                    if (memory.Read((int)Offsets.Stats.AttackMain, 16).ConvertToInt16() == 0)
                        return memory.Read((int)Offsets.Stats.AttackMain2, 16).ConvertToInt16();
                    return memory.Read((int)Offsets.Stats.AttackMain, 16).ConvertToInt16();
                }
            }

            public static int AttackOff
            {
                get
                {
                    if (memory.Read((int)Offsets.Stats.AttackOff, 16).ConvertToInt16() == 0)
                        return memory.Read((int)Offsets.Stats.AttackOff2, 16).ConvertToInt16();
                    return memory.Read((int)Offsets.Stats.AttackOff, 16).ConvertToInt16();
                }
            }

            public static int AccMain
            {
                get { return memory.Read((int)Offsets.Stats.AccMain, 16).ConvertToInt16(); }
            }

            public static int AccOff
            {
                get { return memory.Read((int)Offsets.Stats.AccOff, 16).ConvertToInt16(); }
            }

            public static int CritStrikeMain
            {
                get { return memory.Read((int)Offsets.Stats.CritStrikeMain, 16).ConvertToInt16(); }
            }

            public static int CritStrikeOff
            {
                get { return memory.Read((int)Offsets.Stats.CritStrikeOff, 16).ConvertToInt16(); }
            }

            public static float AttackSpeed
            {
                get { return memory.Read((int)Offsets.Stats.AttackSpeed, 16).ConvertToInt16() / 1000; }
            }

            public static float Speed
            {
                get { return memory.Read((int)Offsets.Stats.Speed, 16).ConvertToSingle(); }
            }

            public static int PhyDef
            {
                get { return memory.Read((int)Offsets.Stats.PhyDef, 16).ConvertToInt16(); }
            }

            public static int Block
            {
                get { return memory.Read((int)Offsets.Stats.Block, 16).ConvertToInt16(); }
            }

            public static int Parry
            {
                get { return memory.Read((int)Offsets.Stats.Parry, 16).ConvertToInt16(); }
            }

            public static int Evansion
            {
                get { return memory.Read((int)Offsets.Stats.Evansion, 16).ConvertToInt16(); }
            }

            public static int CritStrikeResist
            {
                get { return memory.Read((int)Offsets.Stats.CritStrikeResist, 16).ConvertToInt16(); }
            }

            public static int CritStrikeFort
            {
                get { return memory.Read((int)Offsets.Stats.CritStrikeFort, 16).ConvertToInt16(); }
            }

            public static int MagBoost
            {
                get { return memory.Read((int)Offsets.Stats.MagBoost, 16).ConvertToInt16(); }
            }

            public static int MagAcc
            {
                get { return memory.Read((int)Offsets.Stats.MagAcc, 16).ConvertToInt16(); }
            }

            public static int CritSpell
            {
                get { return memory.Read((int)Offsets.Stats.CritSpell, 16).ConvertToInt16(); }
            }

            public static int HealingBoost
            {
                get { return memory.Read((int)Offsets.Stats.HealingBoost, 16).ConvertToInt16(); }
            }

            public static float CastSpeed
            {
                get { return memory.Read((int)Offsets.Stats.CastSpeed, 16).ConvertToSingle(); }
            }

            public static int MagicDef
            {
                get { return memory.Read((int)Offsets.Stats.MagicDef, 16).ConvertToInt16(); }
            }

            public static int MagicSupp
            {
                get { return memory.Read((int)Offsets.Stats.MagicSupp, 16).ConvertToInt16(); }
            }

            public static int MagicResist
            {
                get { return memory.Read((int)Offsets.Stats.MagicResist, 16).ConvertToInt16(); }
            }

            public static int CritSpellResist
            {
                get { return memory.Read((int)Offsets.Stats.CritSpellResist, 16).ConvertToInt16(); }
            }

            public static int SpellFortitude
            {
                get { return memory.Read((int)Offsets.Stats.SpellFortitude, 16).ConvertToInt16(); }
            }

            public static int FireResist
            {
                get { return memory.Read((int)Offsets.Stats.FireResist, 16).ConvertToInt16(); }
            }

            public static int WindResist
            {
                get { return memory.Read((int)Offsets.Stats.WindResist, 16).ConvertToInt16(); }
            }

            public static int WaterResist
            {
                get { return memory.Read((int)Offsets.Stats.WaterResist, 16).ConvertToInt16(); }
            }

            public static int EarthResist
            {
                get { return memory.Read((int)Offsets.Stats.EarthResist, 16).ConvertToInt16(); }
            }

            public static int RankNumber
            {
                get { return memory.Read((int)Offsets.Stats.RankNumber, 16).ConvertToInt16(); }
            }

            public static int Rank
            {
                get { return memory.Read((int)Offsets.Stats.Rank, 16).ConvertToInt16(); }
            }
            public class Kill
            {
                public static int Today
                {
                    get { return memory.Read((int)Offsets.Stats.TodayEly, 16).ConvertToInt16(); }
                }

                public static int ThisWeek
                {
                    get { return memory.Read((int)Offsets.Stats.ThisWeekEly, 16).ConvertToInt16(); }
                }

                public static int LastWeek
                {
                    get { return memory.Read((int)Offsets.Stats.LastWeekEly, 16).ConvertToInt16(); }
                }

                public static int Total
                {
                    get { return memory.Read((int)Offsets.Stats.TotalEly, 16).ConvertToInt16(); }
                }
            }
            public class AbyssPoint
            {
                public static int Total
                {
                    get { return memory.Read((int)Offsets.Stats.TotalAp, 16).ConvertToInt16(); }
                }

                public static int Today
                {
                    get { return memory.Read((int)Offsets.Stats.TodayAp, 16).ConvertToInt16(); }
                }

                public static int ThisWeek
                {
                    get { return memory.Read((int)Offsets.Stats.ThisWeekAp, 16).ConvertToInt16(); }
                }

                public static int LastWeek
                {
                    get { return memory.Read((int)Offsets.Stats.LastWeekAp, 16).ConvertToInt16(); }
                }
            }
            public class HonorPoint
            {
                public static int Total
                {
                    get { return memory.Read((int)Offsets.Stats.TotalHp, 16).ConvertToInt16(); }
                }

                public static int Today
                {
                    get { return memory.Read((int)Offsets.Stats.TodayHp, 16).ConvertToInt16(); }
                }

                public static int ThisWeek
                {
                    get { return memory.Read((int)Offsets.Stats.ThisWeekHp, 16).ConvertToInt16(); }
                }

                public static int LastWeek
                {
                    get { return memory.Read((int)Offsets.Stats.LastWeekHp, 16).ConvertToInt16(); }
                }
            }


        }
        public class Buff
        {
            public static int Count
            {
                get { return memory.Read(new int[] { (int)Offsets.Buff.Pointer, (int)Offsets.Buff.Count }).ConvertToInt32(); }
            }

            public static List<int> ListID
            {
                get
                {
                    List<int> id = new List<int>();
                    for (int i = 0; i < Count; i++)
                    {
                        id.Add(memory.Read(new int[] { (int)Offsets.Buff.Pointer, (int)Offsets.Buff.Offset1 + (i * 4), (int)Offsets.Buff.Offset2 }).ConvertToInt32());
                    }
                    return id;
                }
            }

        }

        public class Skillbar
        {

            public static int GetID(int skillbar, int row, int column)
            {
                int offset = (int)Offsets.Skillbar.FirstIteam + ((skillbar - 1) * (int)Offsets.Skillbar.NextBar);
                offset = offset + ((row - 1) * (int)Offsets.Skillbar.NextRow);
                offset = offset + ((column - 1) * (int)Offsets.Skillbar.NextSkill);
                //   Console.WriteLine("offset " + offset);
                if (memory.Read(offset).ConvertToInt32() == memory.Read(offset + 4).ConvertToInt32())
                {
                    // Console.WriteLine("ID " + memory.Read(offset).ConvertToInt32());
                    return memory.Read(offset).ConvertToInt32();
                }
                return 0;
            }
            public static void SetID(int skillbar, int row, int column, int value)
            {
                int offset = (int)Offsets.Skillbar.FirstIteam + ((skillbar - 1) * (int)Offsets.Skillbar.NextBar);
                offset = offset + ((row - 1) * (int)Offsets.Skillbar.NextRow);
                offset = offset + ((column - 1) * (int)Offsets.Skillbar.NextSkill);
                memory.Write(offset, value);
                //memory.Write(offset +0x4, value);
            }

            public static SkillbarIteam ListID
            {
                get
                {
                    return new SkillbarIteam();
                }
                //set
                //{
                //    memory.Write((int)Offsets.Player.SkillbarNumber, value - 1);
                //}
            }
        }










    }
}
