﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using MyBotModern.MB.GetKey;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Xml;

namespace MyBotModern
{
    public class SettingXML
    {
        public static string defaultpath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\" + Value.Player.Name + ".xml";


        public static void SaveSettingIni(string path)
        {
            XmlWriterSettings writerSettings = new XmlWriterSettings();
            writerSettings.Indent = true;
            using (XmlWriter writer = XmlWriter.Create(path, writerSettings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Setting");
                writer.WriteStartElement("Waypoint");
                writer.WriteElementString(Program.bot.TBactionway.Name, Program.bot.TBactionway.Text);
                writer.WriteElementString(Program.bot.TBdeathway.Name, Program.bot.TBdeathway.Text);
                writer.WriteEndElement();
                writer.WriteStartElement("Key");
                writer.WriteElementString(Program.bot.CBforward.Name, Program.bot.CBforward.SelectedItem.ToString());
                writer.WriteElementString(Program.bot.CBselect.Name, Program.bot.CBselect.SelectedItem.ToString());
                writer.WriteElementString(Program.bot.CBloot.Name, Program.bot.CBloot.SelectedItem.ToString());
                writer.WriteElementString(Program.bot.CBrest.Name, Program.bot.CBrest.SelectedItem.ToString());
                writer.WriteElementString(Program.bot.CBselectyourself.Name, Program.bot.CBselectyourself.SelectedItem.ToString());
                writer.WriteEndElement();
                writer.WriteStartElement("Skills");
                for (int i = 0; i < Skill.list.Count; i++)
                {
                    writer.WriteStartElement("NR", i.ToString());
                    writer.WriteElementString("Skillbar", Skill.list[i].Skillbar.ToString());
                    writer.WriteElementString("Key", Skill.list[i].Key.ToString());
                    writer.WriteElementString("Cooldown", Skill.list[i].Cooldown.ToString());
                    writer.WriteElementString("CastTime", Skill.list[i].CastTime.ToString());
                    writer.WriteElementString("ChainLevel", Skill.list[i].ChainLevel.ToString());
                    writer.WriteElementString("ChainWait", Skill.list[i].ChainWait.ToString());
                    writer.WriteElementString("Date", Skill.list[i].Date.ToString());
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteStartElement("Buffs");
                for (int i = 0; i < Buff.list.Count; i++)
                {
                    writer.WriteStartElement("NR", i.ToString());
                    writer.WriteElementString("Skillbar", Buff.list[i].Skillbar.ToString());
                    writer.WriteElementString("Key", Buff.list[i].Key.ToString());
                    writer.WriteElementString("Cooldown", Buff.list[i].Cooldown.ToString());
                    writer.WriteElementString("CastTime", Buff.list[i].CastTime.ToString());
                    writer.WriteElementString("ChainLevel", Buff.list[i].ChainLevel.ToString());
                    writer.WriteElementString("ChainWait", Buff.list[i].ChainWait.ToString());
                    writer.WriteElementString("OnCombat", Buff.list[i].OnCombat.ToString());
                    writer.WriteElementString("Date", Buff.list[i].Date.ToString());
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();

                writer.WriteStartElement("Potion");
                for (int i = 0; i < Potion.list.Count; i++)
                {
                    writer.WriteStartElement("NR", i.ToString());
                    writer.WriteElementString("Skillbar", Potion.list[i].Skillbar.ToString());
                    writer.WriteElementString("Key", Potion.list[i].Key.ToString());
                    writer.WriteElementString("Cooldown", Potion.list[i].Cooldown.ToString());
                    writer.WriteElementString("CastTime", Potion.list[i].CastTime.ToString());
                    writer.WriteElementString("Effect", Potion.list[i].Effect.ToString());
                    writer.WriteElementString("Percent", Potion.list[i].Percent.ToString());
                    writer.WriteElementString("Date", Potion.list[i].Date.ToString());
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();



                writer.WriteEndDocument();
            }




        }

        public static void LoadSettingIni(string path)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(path);
            foreach (XmlNode child in doc.DocumentElement.SelectSingleNode("Waypoint").ChildNodes)
            {
                switch (child.Name)
                {
                    case "TBactionway":
                        Program.bot.TBactionway.Text = child.InnerText;
                        break;
                    case "TBdeathway":
                        Program.bot.TBdeathway.Text = child.InnerText;
                        break;
                }
            }
            foreach (XmlNode child in doc.DocumentElement.SelectSingleNode("Key").ChildNodes)
            {
                switch (child.Name)
                {
                    case "CBforward":
                        Program.bot.CBforward.SelectedItem = child.InnerText.ToEnum<Key>();
                        break;
                    case "CBselect":
                        Program.bot.CBselect.SelectedItem = child.InnerText.ToEnum<Key>();
                        break;
                    case "CBloot":
                        Program.bot.CBloot.SelectedItem = child.InnerText.ToEnum<Key>();
                        break;
                    case "CBrest":
                        Program.bot.CBrest.SelectedItem = child.InnerText.ToEnum<Key>();
                        break;
                    case "CBselectyourself":
                        Program.bot.CBselectyourself.SelectedItem = child.InnerText.ToEnum<Key>();
                        break;
                    case "CBfly":
                        Program.bot.CBfly.SelectedItem = child.InnerText.ToEnum<Key>();
                        break;
                    case "CBland":
                        Program.bot.CBland.SelectedItem = child.InnerText.ToEnum<Key>();
                        break;
                }
            }
            foreach (XmlNode child in doc.DocumentElement.SelectSingleNode("Skills").ChildNodes)
            {
                Skill.list.Add(new SkillsItem(Convert.ToInt32(child.ChildNodes[0].InnerText),
                child.ChildNodes[1].InnerText.ToEnum<Key>(),
                Convert.ToInt32(child.ChildNodes[2].InnerText),
                Convert.ToInt32(child.ChildNodes[3].InnerText),
                Convert.ToInt32(child.ChildNodes[4].InnerText),
                Convert.ToInt32(child.ChildNodes[5].InnerText)));
            }
            foreach (XmlNode child in doc.DocumentElement.SelectSingleNode("Buffs").ChildNodes)
            {
                Buff.list.Add(new BuffItem(Convert.ToInt32(child.ChildNodes[0].InnerText),
                child.ChildNodes[1].InnerText.ToEnum<Key>(),
                Convert.ToInt32(child.ChildNodes[2].InnerText),
                Convert.ToInt32(child.ChildNodes[3].InnerText),
                Convert.ToInt32(child.ChildNodes[4].InnerText),
                Convert.ToInt32(child.ChildNodes[5].InnerText),
                Convert.ToBoolean(child.ChildNodes[6].InnerText)));
            }
            foreach (XmlNode child in doc.DocumentElement.SelectSingleNode("Potion").ChildNodes)
            {
                Potion.list.Add(new PotionItem(Convert.ToInt32(child.ChildNodes[0].InnerText),
                child.ChildNodes[1].InnerText.ToEnum<Key>(),
                Convert.ToInt32(child.ChildNodes[2].InnerText),
                Convert.ToInt32(child.ChildNodes[3].InnerText),
                child.ChildNodes[4].InnerText.ToEnum<Enums.PotionEffect>(),
                Convert.ToInt32(child.ChildNodes[5].InnerText)));
            }
        }

        #region Default Setting
        public static void LoadDefaultSetting()
        {
            Program.bot.TBactionway.Text = Properties.Settings.Default.TBcombatway;
            Program.bot.TBdeathway.Text = Properties.Settings.Default.TBdeathway;
            Program.bot.CBforward.SelectedIndex = Properties.Settings.Default.CBforward;
            Program.bot.CBloot.SelectedIndex = Properties.Settings.Default.CBloot;

            Program.bot.CBrest.SelectedIndex = Properties.Settings.Default.CBrest;
            Program.bot.CBselect.SelectedIndex = Properties.Settings.Default.CBselect;
            Program.bot.CBselectyourself.SelectedIndex = Properties.Settings.Default.CBselectyourself;
            Program.bot.CBfly.SelectedIndex = Properties.Settings.Default.CBfly;
            Program.bot.CBland.SelectedIndex = Properties.Settings.Default.CBland;
        }


        //public static void SaveDefaultSetting()
        //{
        //    Properties.Settings.Default.TBcombatway = Program.bot.TBactionway.Text;
        //    Properties.Settings.Default.TBdeathway = Program.bot.TBdeathway.Text;

        //    Properties.Settings.Default.CBforward = Program.bot.CBforward.SelectedIndex;
        //    Properties.Settings.Default.CBloot = Program.bot.CBloot.SelectedIndex;
        //    Properties.Settings.Default.CBrest = Program.bot.CBrest.SelectedIndex;
        //    Properties.Settings.Default.CBselect = Program.bot.CBselect.SelectedIndex;
        //    Properties.Settings.Default.CBselectyourself = Program.bot.CBselectyourself.SelectedIndex;
        //    Properties.Settings.Default.CBfly = Program.bot.CBfly.SelectedIndex;
        //    Properties.Settings.Default.CBland = Program.bot.CBland.SelectedIndex;

        //    Properties.Settings.Default.Save();
        //}
        #endregion Default Setting 


    }

    namespace MB.GetKey
    {
        public static class GetKey
        {
            public static T ToEnum<T>(this string value)
            {
                return (T)Enum.Parse(typeof(T), value, true);
            }
            public static string ConvertToString(this int value)
            {
                return Enum.GetName(typeof(Key), value);
            }

            public static int ConvertToInt(this string str)
            {
                return (int)Enum.Parse(typeof(Key), str);
            }
            public static Key ConvertToKey(this string str)
            {
                return (Key)(int)Enum.Parse(typeof(Key), str);
            }
        }
    }
}