﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MyBotModern
{
    /// <summary>
    /// Logika interakcji dla klasy TargetInfo.xaml
    /// </summary>
    public partial class TargetInfo : Window
    {
        private System.Windows.Threading.DispatcherTimer timer1 = new System.Windows.Threading.DispatcherTimer();

        public TargetInfo()
        {
            timer1.Tick += timer1_Tick;
            timer1.Interval = new TimeSpan(0, 0, 0, 0, 100);
            InitializeComponent();
            this.Topmost = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            TargetName.Text = Value.Target.Lvl + " - " + Value.Target.Name;
            DRWtargethp.Maximum = Value.Target.HP.Max;
            DRWtargethp.Value = Value.Target.HP.Cur;
            DRWtargetmp.Maximum = Value.Target.MP.Max;
            DRWtargetmp.Value = Value.Target.MP.Cur;

            DRWtargethp.Text = Value.Target.HP.Cur.ToString("N0") + " / " + Value.Target.HP.Max.ToString("N0");
            DRWtargetmp.Text = Value.Target.MP.Cur.ToString("N0") + " / " + Value.Target.MP.Max.ToString("N0");
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            timer1.Stop();
        }

        private void Window_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.IsVisible)
                timer1.Start();
            else
                timer1.Stop();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ButtonState == e.LeftButton && (bool)CBmove.IsChecked)
                this.DragMove();
        }

        private void SetLoc(Point p)
        {
            var location = this.PointToScreen(p);
            this.Left = location.X;
            this.Top = location.Y - this.Height;

        }

        private void MenuItem20_Click(object sender, RoutedEventArgs e)
        {
            this.Opacity = .20;
        }

        private void MenuItem40_Click(object sender, RoutedEventArgs e)
        {
            this.Opacity = .40;
        }

        private void MenuItem60_Click(object sender, RoutedEventArgs e)
        {
            this.Opacity = .60;
        }

        private void MenuItem80_Click(object sender, RoutedEventArgs e)
        {
            this.Opacity = .80;
        }

        private void MenuItem100_Click(object sender, RoutedEventArgs e)
        {
            this.Opacity = 1;
        }

        private void CBTransparent_Click(object sender, RoutedEventArgs e)
        {
            if ((bool)CBTransparent.IsChecked)
            {
                this.Background = Brushes.Transparent;
            }
            else
            {
                this.Background = Brushes.Black;
            }
        }
    }
}

