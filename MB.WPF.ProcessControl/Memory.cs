﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace MB.WPF.ProcessControl
{
    public class Memory
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] lpBuffer, uint nSize, out int lpNumberOfBytesWritten);
        [DllImport("kernel32.dll")]
        private static extern bool ReadProcessMemory(int hProcess, int lpBaseAddress, byte[] lpBuffer, int dwSize, ref int lpNumberOfBytesRead);

        public Memory(Process process, string ModuleBaseAddress)
        {
            Processes.Set(process, ModuleBaseAddress);
        }
        public Memory(string processName, string ModuleName, int ProcessNumber = 0)
        {
            Processes.Set(processName, ModuleName, ProcessNumber);
        }

        #region Write
        public void Write(int offset, double i)
        {
            int bytesWritten;
            var buffer = BitConverter.GetBytes(i);
            WriteProcessMemory(Processes.processHandle, IntPtr.Add(Processes.baseAddress, (int)offset), buffer, (uint)buffer.Length, out bytesWritten);
        }
        public void Write(int offset, float i)
        {
            int bytesWritten;
            var buffer = BitConverter.GetBytes(i);
            WriteProcessMemory(Processes.processHandle, IntPtr.Add(Processes.baseAddress, (int)offset), buffer, (uint)buffer.Length, out bytesWritten);
        }
        public void Write(int offset, int i)
        {
            int bytesWritten;
            var buffer = BitConverter.GetBytes(i);
            WriteProcessMemory(Processes.processHandle, IntPtr.Add(Processes.baseAddress, (int)offset), buffer, (uint)buffer.Length, out bytesWritten);
        }
        public void Write(int offset, string i)
        {
            int bytesWritten;
            byte[] buffer = Encoding.Unicode.GetBytes(i);
            WriteProcessMemory(Processes.processHandle, IntPtr.Add(Processes.baseAddress, (int)offset), buffer, (uint)buffer.Length, out bytesWritten);
        }

        public void Write(int[] offsets, int i)
        {
            int Base = offsets[0];
            int bytesRead = 0;
            int bytesWritten;
            byte[] buffer = new byte[32];
            byte[] buffer1 = BitConverter.GetBytes(i);
            if (offsets.Length > 1)
            {
                ReadProcessMemory((int)Processes.processHandle, IntPtr.Add(Processes.baseAddress, offsets[0]).ToInt32(), buffer, buffer.Length, ref bytesRead);
                for (int ij = 1; ij < (offsets.Length) - 1; ij++)
                {
                    Base = BitConverter.ToInt32(buffer, 0) + offsets[ij];
                    ReadProcessMemory((int)Processes.processHandle, Base, buffer, buffer.Length, ref bytesRead);
                }
                Base = BitConverter.ToInt32(buffer, 0) + offsets[offsets.Length - 1];
                WriteProcessMemory(Processes.processHandle, (IntPtr)Base, buffer1, (uint)buffer1.Length, out bytesWritten);
            }
            else
                WriteProcessMemory(Processes.processHandle, IntPtr.Add(Processes.baseAddress, Base), buffer1, (uint)buffer1.Length, out bytesWritten);
        }
        #endregion Write

        #region Read
        public byte[] Read(int[] offsets, int length = 32)
        {
            int bytesRead = 0;
            byte[] buffer = new byte[length];
            ReadProcessMemory((int)Processes.processHandle, IntPtr.Add(Processes.baseAddress, offsets[0]).ToInt32(), buffer, buffer.Length, ref bytesRead);
            for (int i = 1; i < offsets.Length; i++)
            {
                int Base = BitConverter.ToInt32(buffer, 0) + offsets[i];
                ReadProcessMemory((int)Processes.processHandle, Base, buffer, buffer.Length, ref bytesRead);
            }
            return buffer;
        }
        public byte[] Read(int offset, int length = 32)
        {
            int bytesRead = 0;
            byte[] buffer = new byte[length];
            ReadProcessMemory((int)Processes.processHandle, IntPtr.Add(Processes.baseAddress, offset).ToInt32(), buffer, buffer.Length, ref bytesRead);
            return buffer;
        }
        #endregion Read


    }

    public static class ConvertMemory
    {
        static char[] alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz 1234567890".ToCharArray();
        public static int ConvertToByte(this byte[] buffer)
        {
            return buffer[0];
        }
        public static string ConvertToString(this byte[] buffer)
        {
            string str = Encoding.Unicode.GetString(buffer);
            return str.Remove(str.Length - str.Trim(alpha).Length);
        }
        public static int ConvertToInt16(this byte[] buffer)
        {
            return BitConverter.ToInt16(buffer, 0);
        }
        public static int ConvertToInt32(this byte[] buffer)
        {
            return BitConverter.ToInt32(buffer, 0);
        }
        public static float ConvertToSingle(this byte[] buffer, bool round = false)
        {
            if (round)
                return (float)Math.Round(BitConverter.ToSingle(buffer, 0), 1, MidpointRounding.AwayFromZero);
            else
                return (float)BitConverter.ToSingle(buffer, 0);
        }

        public static string ConvertToRemovedCharactersString(this byte[] buffer)
        {
            string str = Encoding.Unicode.GetString(buffer);
            return Regex.Replace(Encoding.Unicode.GetString(buffer).Remove(str.Length - str.Trim(alpha).Length), "[A-Za-z ]", String.Empty);
        }
        public static string ConvertToRemovedNumbersString(this byte[] buffer)
        {
            string str = Encoding.Unicode.GetString(buffer);
            return Regex.Replace(Encoding.Unicode.GetString(buffer).Remove(str.Length - str.Trim(alpha).Length), @"[\d-]", String.Empty);
        }
    }
}