﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MB.WPF.ProcessControl
{
    public class Keybord
    {
        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern bool SendMessage(IntPtr hWnd, int wMsg, uint wParam, uint lParam);

        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool PostMessage(IntPtr hWnd, int Msg, uint wParam, uint lParam);

        public enum Message : int
        {
            /// <summary>Key down</summary>
            KEY_DOWN = (0x0100),
            /// <summary>Key up</summary>
            KEY_UP = (0x0101),
            /// <summary>The character being pressed</summary>
            VM_CHAR = (0x0102),
            /// <summary>An Alt/ctrl/shift + key down message</summary>
            SYSKEYDOWN = (0x0104),
            /// <summary>An Alt/Ctrl/Shift + Key up Message</summary>
            SYSKEYUP = (0x0105),
            /// <summary>An Alt/Ctrl/Shift + Key character Message</summary>
            SYSCHAR = (0x0106),
            /// <summary>Left mousebutton down</summary>
            LBUTTONDOWN = (0x201),
            /// <summary>Left mousebutton up</summary>
            LBUTTONUP = (0x202),
            /// <summary>Left mousebutton double left click</summary>
            LBUTTONDBLCLK = (0x203),
            /// <summary>Right mousebutton down</summary>
            RBUTTONDOWN = (0x204),
            /// <summary>Right mousebutton up</summary>
            RBUTTONUP = (0x205),
            /// <summary>Right mousebutton doubleclick</summary>
            RBUTTONDBLCLK = (0x206)
        }

        private IntPtr processHandle;

        public Keybord(Process process)
        {
            SetProcess(process);
        }
        public Keybord(string process, int nr = 0)
        {
            SetProcess(process, nr);
        }
        public Keybord(IntPtr intptr)
        {
            processHandle = intptr;
        }

        public void SetProcess(string process, int nr = 0)
        {
            Process[] ProcessList = Process.GetProcessesByName(process);
            if (ProcessList.Length != 0)
            {
                processHandle = Process.GetProcessById(ProcessList[nr].Id).MainWindowHandle;
            }
        }

        public void SetProcess(Process proc)
        {
            processHandle = Process.GetProcessById(proc.Id).MainWindowHandle;
        }

        #region PostMessage
        public void PostMessage(Key key)
        {
            PostMessageDown(key);
            PostMessageUp(key);
        }
        public void PostMessage(Key[] key)
        {
            for (int i = 0; i < key.Length; i++)
            {
                PostMessageDown(key[i]);
            }
            for (int i = key.Length - 1; i >= 0; i--)
            {
                PostMessageUp(key[i]);
            }
        }
        public void PostMessageDown(Key key)
        {
            PostMessage(processHandle, (int)Message.KEY_DOWN, (uint)KeyInterop.VirtualKeyFromKey(key), (uint)0);
        }
        public void PostMessageUp(Key key)
        {
            PostMessage(processHandle, (int)Message.KEY_UP, (uint)KeyInterop.VirtualKeyFromKey(key), (uint)0);
        }
        #endregion PostMessage

        #region SendMessage
        public void SendMessage(Key key)
        {
            SendMessageDown(key);
            SendMessageUp(key);
        }
        public void SendMessage(string key)
        {
            SendMessageDown(key);
            SendMessageUp(key);
        }

        public void SendMessage(Key[] key)
        {
            for (int i = 0; i < key.Length; i++)
            {
                SendMessageDown(key[i]);
            }
            for (int i = key.Length - 1; i >= 0; i--)
            {
                SendMessageUp(key[i]);
            }
        }

        public void SendMessageDown(Key key)
        {
            SendMessage(processHandle, (int)Message.KEY_DOWN, (uint)KeyInterop.VirtualKeyFromKey(key), (uint)0);
        }

        public void SendMessageUp(Key key)
        {
            SendMessage(processHandle, (int)Message.KEY_UP, (uint)KeyInterop.VirtualKeyFromKey(key), (uint)0);
        }

        public void SendMessageDown(string key)
        {
            SendMessage(processHandle, (int)Message.KEY_DOWN, (uint)KeyInterop.VirtualKeyFromKey((Key)Enum.Parse(typeof(Key), key)), (uint)0);
        }

        public void SendMessageUp(string key)
        {
            SendMessage(processHandle, (int)Message.KEY_UP, (uint)KeyInterop.VirtualKeyFromKey((Key)Enum.Parse(typeof(Key), key)), (uint)0);
        }
        #endregion SendMessage

        #region Keybd_event
        [DllImport("user32.dll", SetLastError = true)]
        private static extern void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);

        private const int KEYEVENTF_EXTENDEDKEY = 0x0001;
        private const int KEYEVENTF_KEYUP = 0x0002;

        public void keybd_eventDown(VKeys key)
        {
            if (WindowControl.GetForegroundWindow() == processHandle)
                keybd_event((byte)key, 0, KEYEVENTF_EXTENDEDKEY, 0);
            else
            {
                WindowControl.SetForegroundWindow(processHandle);
                keybd_event((byte)key, 0, KEYEVENTF_EXTENDEDKEY, 0);
            }
        }
        public void keybd_eventUp(VKeys key)
        {
            if (WindowControl.GetForegroundWindow() == processHandle)
                keybd_event((byte)key, 0, KEYEVENTF_KEYUP, 0);
                        else
            {
                WindowControl.SetForegroundWindow(processHandle);
                keybd_event((byte)key, 0, KEYEVENTF_EXTENDEDKEY, 0);
            }
        }
        public void KeybdEvent(VKeys key)
        {
            keybd_eventDown(key);
            keybd_eventUp(key);
        }
        public void KeybdEventWithAlt(VKeys key)
        {
            keybd_eventDown(VKeys.KEY_MENU);
            keybd_eventDown(key);
            keybd_eventUp(key);
            keybd_eventUp(VKeys.KEY_MENU);
        }
        public void KeybdEventWithCtrl(VKeys key)
        {
            keybd_eventDown(VKeys.KEY_LCONTROL);
            keybd_eventDown(key);
            keybd_eventUp(key);
            keybd_eventUp(VKeys.KEY_LCONTROL);
        }
        public void KeybdEventWithShift(VKeys key)
        {
            keybd_eventDown(VKeys.KEY_LSHIFT);
            keybd_eventDown(key);
            keybd_eventUp(key);
            keybd_eventUp(VKeys.KEY_LSHIFT);
        }

        [Serializable]
        public enum VKeys
        {
            NULL = 0x0,
            /// <summary>Left mouse button</summary>
            KEY_LBUTTON = 0x01,
            /// <summary>Right mouse button</summary>
            KEY_RBUTTON = 0x02,
            /// <summary>Control-break processing</summary>
            KEY_CANCEL = 0x03,
            /// <summary>Middle mouse button (three-button mouse)</summary>
            KEY_MBUTTON = 0x04,
            /// <summary>BACKSPACE key</summary>
            KEY_BACK = 0x08,
            /// <summary>TAB key</summary>
            KEY_TAB = 0x09,
            /// <summary>CLEAR key</summary>
            KEY_CLEAR = 0x0C,
            /// <summary>ENTER key</summary>
            KEY_RETURN = 0x0D,
            /// <summary>SHIFT key</summary>
            KEY_SHIFT = 0x10,
            /// <summary>CTRL key</summary>
            KEY_CONTROL = 0x11,
            //<summary>LALT key FT</summary>
            KEY_MENU = 0x12,
            /// <summary>PAUSE key</summary>
            KEY_PAUSE = 0x13,
            /// <summary>CAPS LOCK key</summary>
            KEY_CAPITAL = 0x14,
            /// <summary>ESC key</summary>
            KEY_ESCAPE = 0x1B,
            /// <summary>SPACEBAR</summary>
            KEY_SPACE = 0x20,
            /// <summary>PAGE UP key</summary>
            KEY_PRIOR = 0x21,
            /// <summary>PAGE DOWN key</summary>
            KEY_NEXT = 0x22,
            /// <summary>END key</summary>
            KEY_END = 0x23,
            /// <summary>HOME key</summary>
            KEY_HOME = 0x24,
            /// <summary>LEFT ARROW key</summary>
            KEY_LEFT = 0x25,
            /// <summary>UP ARROW key</summary>
            KEY_UP = 0x26,
            /// <summary>RIGHT ARROW key</summary>
            KEY_RIGHT = 0x27,
            /// <summary>DOWN ARROW key</summary>
            KEY_DOWN = 0x28,
            /// <summary>SELECT key</summary>
            KEY_SELECT = 0x29,
            /// <summary>PRINT key</summary>
            KEY_PRINT = 0x2A,
            /// <summary>EXECUTE key</summary>
            KEY_EXECUTE = 0x2B,
            /// <summary>PRINT SCREEN key</summary>
            KEY_SNAPSHOT = 0x2C,
            /// <summary>INS key</summary>
            KEY_INSERT = 0x2D,
            /// <summary>DEL key</summary>
            KEY_DELETE = 0x2E,
            /// <summary>HELP key</summary>
            KEY_HELP = 0x2F,
            /// <summary>0 key</summary>
            KEY_0 = 0x30,
            /// <summary>1 key</summary>
            KEY_1 = 0x31,
            /// <summary>2 key</summary>
            KEY_2 = 0x32,
            /// <summary>3 key</summary>
            KEY_3 = 0x33,
            /// <summary>4 key</summary>
            KEY_4 = 0x34,
            /// <summary>5 key</summary>
            KEY_5 = 0x35,
            /// <summary>6 key</summary>
            KEY_6 = 0x36,
            /// <summary>7 key</summary>
            KEY_7 = 0x37,
            /// <summary>8 key</summary>
            KEY_8 = 0x38,
            /// <summary>9 key</summary>
            KEY_9 = 0x39,
            /// <summary> - key</summary>
            KEY_MINUS = 0xBD,
            /// <summary>+ key</summary>
            KEY_PLUS = 0xBB,
            /// <summary> A key</summary>
            KEY_A = 0x41,
            /// <summary>B key</summary>
            KEY_B = 0x42,
            /// <summary>C key</summary>
            KEY_C = 0x43,
            /// <summary>D key</summary>
            KEY_D = 0x44,
            /// <summary>E key</summary>
            KEY_E = 0x45,
            /// <summary>F key</summary>
            KEY_F = 0x46,
            /// <summary>G key</summary>
            KEY_G = 0x47,
            /// <summary>H key</summary>
            KEY_H = 0x48,
            /// <summary>I key</summary>
            KEY_I = 0x49,
            /// <summary>J key</summary>
            KEY_J = 0x4A,
            /// <summary>K key</summary>
            KEY_K = 0x4B,
            /// <summary>L key</summary>
            KEY_L = 0x4C,
            /// <summary>M key</summary>
            KEY_M = 0x4D,
            /// <summary>N key</summary>
            KEY_N = 0x4E,
            /// <summary>O key</summary>
            KEY_O = 0x4F,
            /// <summary>P key</summary>
            KEY_P = 0x50,
            /// <summary>Q key</summary>
            KEY_Q = 0x51,
            /// <summary>R key</summary>
            KEY_R = 0x52,
            /// <summary>S key</summary>
            KEY_S = 0x53,
            /// <summary>T key</summary>
            KEY_T = 0x54,
            /// <summary>U key</summary>
            KEY_U = 0x55,
            /// <summary>V key</summary>
            KEY_V = 0x56,
            /// <summary>W key</summary>
            KEY_W = 0x57,
            /// <summary>X key</summary>
            KEY_X = 0x58,
            /// <summary>Y key</summary>
            KEY_Y = 0x59,
            /// <summary>Z key</summary>
            KEY_Z = 0x5A,
            /// <summary>Numeric keypad 0 key</summary>
            KEY_NUMPAD0 = 0x60,
            /// <summary>Numeric keypad 1 key</summary>
            KEY_NUMPAD1 = 0x61,
            /// <summary>Numeric keypad 2 key</summary>
            KEY_NUMPAD2 = 0x62,
            /// <summary>Numeric keypad 3 key</summary>
            KEY_NUMPAD3 = 0x63,
            /// <summary>Numeric keypad 4 key</summary>
            KEY_NUMPAD4 = 0x64,
            /// <summary>Numeric keypad 5 key</summary>
            KEY_NUMPAD5 = 0x65,
            /// <summary>Numeric keypad 6 key</summary>
            KEY_NUMPAD6 = 0x66,
            /// <summary>Numeric keypad 7 key</summary>
            KEY_NUMPAD7 = 0x67,
            /// <summary>Numeric keypad 8 key</summary>
            KEY_NUMPAD8 = 0x68,
            /// <summary> Numeric keypad 9 key</summary>
            KEY_NUMPAD9 = 0x69,
            /// <summary>Separator key</summary>
            KEY_SEPARATOR = 0x6C,
            /// <summary>Subtract key</summary>
            KEY_SUBTRACT = 0x6D,
            /// <summary>Decimal key</summary>
            KEY_DECIMAL = 0x6E,
            /// <summary>Divide key</summary>
            KEY_DIVIDE = 0x6F,
            /// <summary>F1 key</summary>
            KEY_F1 = 0x70,
            /// <summary>F2 key</summary>
            KEY_F2 = 0x71,
            /// <summary>F3 key</summary>
            KEY_F3 = 0x72,
            /// <summary>F4 key</summary>
            KEY_F4 = 0x73,
            /// <summary>F5 key</summary>
            KEY_F5 = 0x74,
            /// <summary>F6 key</summary>
            KEY_F6 = 0x75,
            /// <summary>F7 key</summary>
            KEY_F7 = 0x76,
            /// <summary>F8 key</summary>
            KEY_F8 = 0x77,
            /// <summary>F9 key</summary>
            KEY_F9 = 0x78,
            /// <summary>F10 key</summary>
            KEY_F10 = 0x79,
            /// <summary>F11 key</summary>
            KEY_F11 = 0x7A,
            /// <summary>F12 key</summary>
            KEY_F12 = 0x7B,
            /// <summary>SCROLL LOCK key</summary>
            KEY_SCROLL = 0x91,
            /// <summary> Left SHIFT key</summary>
            KEY_LSHIFT = 0xA0,
            /// <summary>Right SHIFT key</summary>
            KEY_RSHIFT = 0xA1,
            /// <summary>Left CONTROL key</summary>
            KEY_LCONTROL = 0xA2,
            /// <summary>Right CONTROL key</summary>
            KEY_RCONTROL = 0xA3,
            /// <summary>Left MENU key</summary>
            KEY_LMENU = 0xA4,
            /// <summary>Right MENU key</summary>
            KEY_RMENU = 0xA5,
            /// <summary>, key</summary>
            KEY_COMMA = 0xBC,
            /// <summary>. key</summary>
            KEY_PERIOD = 0xBE,
            /// <summary>Play key</summary>
            KEY_PLAY = 0xFA,
            /// <summary>Zoom key</summary>
            KEY_ZOOM = 0xFB,
            #endregion Keybd_event
        }
    }
}