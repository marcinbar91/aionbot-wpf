﻿using System.Diagnostics;
using System.Runtime.InteropServices;

namespace MB.WPF.ProcessControl
{
    public class System
    {
        [DllImport("user32")]
        public static extern bool ExitWindowsEx(uint uFlags, uint dwReason);
        [DllImport("user32")]
        public static extern void LockWorkStation();
        [DllImport("PowrProf.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern bool SetSuspendState(bool hiberate, bool forceCritical, bool disableWakeEvent);

        public void Shutdown()
        {
            var process = new ProcessStartInfo("shutdown", "/s /t 0");
            process.CreateNoWindow = true;
            process.UseShellExecute = false;
            Process.Start(process);
        }

        public void Restart()
        {
            var process = new ProcessStartInfo("shutdown", "/r /t 0");
            process.CreateNoWindow = true;
            process.UseShellExecute = false;
            Process.Start(process);
        }
        public void Logout()
        {
            ExitWindowsEx(0, 0);
        }
        public void Lock()
        {
            LockWorkStation();
        }

        public void Hibernate()
        {
            SetSuspendState(true, true, true);
        }

        public void Sleep()
        {
            SetSuspendState(false, true, true);
        }
    }
}
