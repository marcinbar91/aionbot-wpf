﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace MB.WPF.ProcessControl
{
    public class Processes
    {
        public static Process[] ProcessList;
        public static IntPtr processHandle;
        public static IntPtr baseAddress;

        [DllImport("kernel32.dll")]
        public static extern IntPtr OpenProcess(ProcessAccessFlags dwDesiredAccess, bool bInheritHandle, int dwProcessId);

        [DllImport("kernel32.dll")]
        public static extern Int32 CloseHandle(IntPtr hProcess);

        public enum ProcessAccessFlags : uint
        {
            All = 0x001F0FFF,
            Terminate = 0x00000001,
            CreateThread = 0x00000002,
            VMOperation = 0x00000008,
            VMRead = 0x00000010,
            VMWrite = 0x00000020,
            DupHandle = 0x00000040,
            SetInformation = 0x00000200,
            QueryInformation = 0x00000400,
            Synchronize = 0x00100000
        }

        public static void Set(string processName, string ModuleName, int ProcessNumber = 0)
        {
            ProcessList = Process.GetProcessesByName(processName);
            if (ProcessList.Length != 0)
            {
                processHandle = OpenProcess(ProcessAccessFlags.All, false, ProcessList[ProcessNumber].Id);
                baseAddress = GetModuleBaseAddress(ModuleName, ProcessList[ProcessNumber].Id);
            }
        }
        public static void Set(Process process, string ModuleBaseAddress)
        {
            if (IsRunning(process))
            {
                processHandle = OpenProcess(ProcessAccessFlags.All, false, process.Id);
                baseAddress = GetModuleBaseAddress(ModuleBaseAddress, process.Id);
            }
        }

        public static IntPtr GetModuleBaseAddress(string ModuleName, int currentPID)
        {
            IntPtr BaseAddress = IntPtr.Zero;
            ProcessModule myProcessModule = null;
            ProcessModuleCollection myProcessModuleCollection;

            try
            {
                myProcessModuleCollection = Process.GetProcessById(currentPID).Modules;
            }
            catch { return IntPtr.Zero; }

            for (int i = 0; i < myProcessModuleCollection.Count; i++)
            {
                myProcessModule = myProcessModuleCollection[i];
                if (myProcessModule.ModuleName.Contains(ModuleName))
                {
                    BaseAddress = myProcessModule.BaseAddress;
                    break;
                }
            }

            return BaseAddress;
        }

        public static bool IsRunning(Process process)
        {
            if (process == null)
                throw new ArgumentNullException("process");

            try
            {
                Process.GetProcessById(process.Id);
            }
            catch (ArgumentException)
            {
                return false;
            }
            return true;
        }
    }
}
