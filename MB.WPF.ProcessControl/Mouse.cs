﻿using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows;

namespace MB.WPF.ProcessControl
{
    public class Mouse
    {
        [DllImport("user32.dll", EntryPoint = "SetCursorPos")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetCursorPos(int X, int Y);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetCursorPos(out Point lpMousePoint);

        [DllImport("user32.dll")]
        private static extern void mouse_event(int dwFlags, int dx, int dy, int dwData = 0, int dwExtraInfo = 0);

        public enum MouseEventFlags
        {
            LeftDown = 0x00000002,
            LeftUp = 0x00000004,
            MiddleDown = 0x00000020,
            MiddleUp = 0x00000040,
            Move = 0x00000001,
            Absolute = 0x00008000,
            RightDown = 0x00000008,
            RightUp = 0x00000010
        }

        public Mouse()
        {
        }

        public static void SetCursorPosition(int X, int Y)
        {
            SetCursorPos(X, Y);
        }

        public static void SetCursorPosition(Point point)
        {
            SetCursorPos((int)point.X, (int)point.Y);
        }

        public static Point GetCursorPosition()
        {
            Point currentMousePoint;
            var gotPoint = GetCursorPos(out currentMousePoint);
            if (!gotPoint) { currentMousePoint = new Point(0, 0); }
            return currentMousePoint;
        }

        public static void MouseClickLeft(int x = -1, int y = -1)
        {
            if (x == -1 || y == -1)
            {
                x = (int)GetCursorPosition().X;
                y = (int)GetCursorPosition().Y;
            }
            SetCursorPosition(x, y);
            mouse_event((int)MouseEventFlags.LeftDown, x, y);
            mouse_event((int)MouseEventFlags.LeftUp, x, y);
        }
        public static void MouseClickRight(int x = -1, int y = -1)
        {
            if (x == -1 || y == -1)
            {
                x = (int)GetCursorPosition().X;
                y = (int)GetCursorPosition().Y;
            }
            SetCursorPosition(x, y);
            mouse_event((int)MouseEventFlags.LeftDown, x, y);
            mouse_event((int)MouseEventFlags.LeftUp, x, y);
        }
    }
}
